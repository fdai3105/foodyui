import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/app_style.dart';

class CustomTabBar extends StatelessWidget {
  final List<Tab> tabs;
  final EdgeInsets padding;

  const CustomTabBar({Key key, this.tabs, this.padding}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: padding ?? EdgeInsets.symmetric(vertical: 10),
        child: TabBar(
          tabs: tabs,
          isScrollable: true,
          labelColor: AppStyle.tabSelected.color,
          unselectedLabelColor: AppStyle.tab.color,
          labelStyle: AppStyle.tabSelected,
          unselectedLabelStyle: AppStyle.tab,
          indicatorSize: TabBarIndicatorSize.label,
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              color: Colors.red,
              width: 4,
              style: BorderStyle.solid,
            ),
            insets: EdgeInsets.symmetric(horizontal: 20),
          ),
          labelPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        ),
      ),
    );
  }
}
