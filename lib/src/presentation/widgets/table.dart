import 'dart:math';

import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:hues_foody/src/utils/utils.dart';

enum TableStatus {
  EMPTY,
  BOOKED,
  CAME,
}

class TableWidget extends StatelessWidget {
  final int chairsSlot;
  final TableStatus status;
  final int booked;
  final bool showTest;
  final String desc;

  const TableWidget({
    Key key,
    @required this.chairsSlot,
    @required this.status,
    this.booked = 0,
    this.showTest = false,
    this.desc,
  }) : super(key: key);

  _color() {
    switch (status) {
      case TableStatus.EMPTY:
        return AppColors.colorPrimary;
        break;
      case TableStatus.BOOKED:
        return Colors.yellow;
        break;
      case TableStatus.CAME:
        return Colors.red;
        break;
      default:
        return Colors.cyan;
        break;
    }
  }

  List<Align> _tableItemSmall() {
    Color color;
    final list = <Align>[];
    list.add(Align(
      alignment: Alignment(0, 0),
      child: Container(
        height: 34,
        width: 34,
        decoration: BoxDecoration(
          color: _color(),
          shape: BoxShape.circle,
        ),
        child: showTest
            ? Center(
                child: Text(
                  '$booked/$chairsSlot',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            : null,
      ),
    ));
    for (var i = 0; i <= chairsSlot; i++) {
      if (i <= booked) {
        color = Colors.yellow;
      } else {
        color = _color();
      }

      var degree = 360 / chairsSlot * i;
      var radian = degree * (pi / 180);
      list.add(
        Align(
          alignment: Alignment(sin(radian), cos(radian)),
          child: Transform.rotate(
            angle: -radian,
            child: Image.asset(
              'assets/images/chair.png',
              height: 18,
              width: 18,
              color: color,
            ),
          ),
        ),
      );
    }
    return list;
  }

  List<Align> _tableItemMedium() {
    Color color;
    final list = <Align>[];
    list.add(Align(
      alignment: Alignment(0, 0),
      child: Container(
        height: 80,
        width: 80,
        decoration: BoxDecoration(
          color: _color(),
          shape: BoxShape.circle,
        ),
        child: showTest
            ? Center(
                child: Text(
                  '$booked/$chairsSlot',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            : null,
      ),
    ));
    for (var i = 0; i <= chairsSlot; i++) {
      if (i <= booked) {
        color = Colors.yellow;
      } else {
        color = _color();
      }

      var degree = 360 / chairsSlot * i;
      var radian = degree * (pi / 180);
      list.add(
        Align(
          alignment: Alignment(sin(radian), cos(radian)),
          child: Transform.rotate(
            angle: -radian,
            child: Image.asset(
              'assets/images/chair.png',
              height: 40,
              width: 40,
              color: color,
            ),
          ),
        ),
      );
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Response.isSmallScreen(context)
            ? SizedBox(
                height: 80,
                width: 80,
                child: Stack(
                  children: _tableItemSmall(),
                ),
              )
            : SizedBox(
                height: 180,
                width: 180,
                child: Stack(
                  children: _tableItemMedium(),
                ),
              ),
        desc != null ? SizedBox(height: 4) : SizedBox(),
        desc != null
            ? Text(desc, style: TextStyle(color: Colors.white))
            : SizedBox(),
      ],
    );
  }
}
