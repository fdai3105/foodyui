import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:hues_foody/src/presentation/presentation.dart';
import 'package:hues_foody/src/resource/models/models.dart';
import 'package:hues_foody/src/utils/response.dart';

class MALuotDatTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Response.isSmallScreen(context)
        ? _mobile(context)
        : _table(context);
  }

  _mobile(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.symmetric(horizontal: 20),
            reverse: true,
            child: CustomSelect(
              buttons: _listDate(14),
              onChanged: (value) {},
              defaultSelect: 0,
              selectDecoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.yellow.shade800,
                    width: 4,
                  ),
                ),
              ),
              selectStyle: TextStyle(
                  color: Colors.yellow.shade800,
                  fontSize: 18,
                  fontWeight: FontWeight.w600),
              textStyle: TextStyle(
                color: Colors.black54,
                fontSize: 18,
              ),
              space: 40,
              paddingContent: EdgeInsets.symmetric(horizontal: 6, vertical: 10),
            ),
          ),
          CustomDropdown(
            items: [
              DropdownMenuItem(
                child: Text('Nhà hàng Hoa Mai 1 - Hà Nội'),
                value: 'hanoi',
              ),
              DropdownMenuItem(
                child: Text('Nhà hàng Hoa Mai 2 - Huế'),
                value: 'hue',
              )
            ],
            onChanged: (value) {},
            hint: 'Chọn nhà hàng',
            shadow: true,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color: AppColors.colorSecondary,
            child: Column(
              children: [
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SortButtonWidget(
                      onChanged: (value) {},
                    ),
                    DropdownButtonHideUnderline(
                      child: DropdownButton(
                        items: [
                          DropdownMenuItem(
                            child: Text('Top 5'),
                            value: 5,
                          ),
                          DropdownMenuItem(
                            child: Text('Top 10'),
                            value: 10,
                          ),
                          DropdownMenuItem(
                            child: Text('Top 15'),
                            value: 15,
                          ),
                        ],
                        onChanged: (value) {},
                        value: 5,
                        isDense: true,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                        dropdownColor: AppColors.colorSecondary,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    TopMonAnItem(
                      isFirst: true,
                      leading: '1',
                      title: 'Tôm hùng B',
                      trailing: '200',
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: CustomExpansion(
                            title: 'Thống kê theo ngày',
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: Column(
                                        children: [
                                          DotWidget(
                                            color: Colors.green,
                                            text: 'Hôm Nay',
                                          ),
                                          SizedBox(height: 20),
                                          DotWidget(
                                            text: 'Hôm Qua',
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      flex: 1,
                                      child: BarChartWeekly(
                                        [
                                          Bar(x: 0, y1: 200, y2: 100),
                                        ],
                                        showDot: false,
                                        showGrid: true,
                                        gridStep: 100,
                                        unit: 'Lượt',
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    TopMonAnItem(
                      leading: '2',
                      title: 'Tôm hùng C',
                      trailing: '180',
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: CustomExpansion(
                            title: 'Thống kê theo ngày',
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: Column(
                                        children: [
                                          DotWidget(
                                            color: Colors.green,
                                            text: 'Hôm Nay',
                                          ),
                                          SizedBox(height: 20),
                                          DotWidget(
                                            text: 'Hôm Qua',
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      flex: 1,
                                      child: BarChartWeekly(
                                        [
                                          Bar(x: 0, y1: 200, y2: 100),
                                        ],
                                        showDot: false,
                                        showGrid: true,
                                        gridStep: 100,
                                        unit: 'Lượt',
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    TopMonAnItem(
                      leading: '3',
                      title: 'Tôm hùng D',
                      trailing: '160',
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: CustomExpansion(
                            title: 'Thống kê theo ngày',
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: Column(
                                        children: [
                                          DotWidget(
                                            color: Colors.green,
                                            text: 'Hôm Nay',
                                          ),
                                          SizedBox(height: 20),
                                          DotWidget(
                                            text: 'Hôm Qua',
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      flex: 1,
                                      child: BarChartWeekly(
                                        [
                                          Bar(x: 0, y1: 200, y2: 100),
                                        ],
                                        showDot: false,
                                        showGrid: true,
                                        gridStep: 100,
                                        unit: 'Lượt',
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    TopMonAnItem(
                      leading: '4',
                      title: 'Tôm hùng E',
                      trailing: '140',
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: CustomExpansion(
                            title: 'Thống kê theo ngày',
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: Column(
                                        children: [
                                          DotWidget(
                                            color: Colors.green,
                                            text: 'Hôm Nay',
                                          ),
                                          SizedBox(height: 20),
                                          DotWidget(
                                            text: 'Hôm Qua',
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      flex: 1,
                                      child: BarChartWeekly(
                                        [
                                          Bar(x: 0, y1: 200, y2: 100),
                                        ],
                                        showDot: false,
                                        showGrid: true,
                                        gridStep: 100,
                                        unit: 'Lượt',
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    TopMonAnItem(
                      leading: '5',
                      title: 'Tôm hùng A',
                      trailing: '120',
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: CustomExpansion(
                            title: 'Thống kê theo ngày',
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: Column(
                                        children: [
                                          DotWidget(
                                            color: Colors.green,
                                            text: 'Hôm Nay',
                                          ),
                                          SizedBox(height: 20),
                                          DotWidget(
                                            text: 'Hôm Qua',
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      flex: 1,
                                      child: BarChartWeekly(
                                        [
                                          Bar(x: 0, y1: 200, y2: 100),
                                        ],
                                        showDot: false,
                                        showGrid: true,
                                        gridStep: 100,
                                        unit: 'Lượt',
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 20),
                FlatButton(
                  onPressed: () {},
                  color: Colors.green,
                  minWidth: double.infinity,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Text('Xuất báo cáo'),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _table(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.symmetric(horizontal: 20),
          reverse: true,
          child: CustomSelect(
            buttons: _listDate(14),
            onChanged: (value) {},
            defaultSelect: 0,
            selectDecoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.yellow.shade800,
                  width: 4,
                ),
              ),
            ),
            selectStyle: TextStyle(
                color: Colors.yellow.shade800,
                fontSize: 18,
                fontWeight: FontWeight.w600),
            textStyle: TextStyle(
              color: Colors.black54,
              fontSize: 18,
            ),
            space: 40,
            paddingContent: EdgeInsets.symmetric(horizontal: 6, vertical: 10),
          ),
        ),
        CustomDropdown(
          items: [
            DropdownMenuItem(
              child: Text('Nhà hàng Hoa Mai 1 - Hà Nội'),
              value: 'hanoi',
            ),
            DropdownMenuItem(
              child: Text('Nhà hàng Hoa Mai 2 - Huế'),
              value: 'hue',
            )
          ],
          onChanged: (value) {},
          hint: 'Chọn nhà hàng',
          shadow: true,
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color: AppColors.colorSecondary,
            child: Column(
              children: [
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SortButtonWidget(
                      onChanged: (value) {},
                    ),
                    DropdownButtonHideUnderline(
                      child: DropdownButton(
                        items: [
                          DropdownMenuItem(
                            child: Text('Top 5'),
                            value: 5,
                          ),
                          DropdownMenuItem(
                            child: Text('Top 10'),
                            value: 10,
                          ),
                          DropdownMenuItem(
                            child: Text('Top 15'),
                            value: 15,
                          ),
                        ],
                        onChanged: (value) {},
                        value: 5,
                        isDense: true,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                        dropdownColor: AppColors.colorSecondary,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      TopMonAnItem(
                        isFirst: true,
                        leading: '1',
                        title: 'Tôm hùng B',
                        trailing: '200',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      TopMonAnItem(
                        leading: '2',
                        title: 'Tôm hùng C',
                        trailing: '180',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      TopMonAnItem(
                        leading: '3',
                        title: 'Tôm hùng D',
                        trailing: '160',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      TopMonAnItem(
                        leading: '4',
                        title: 'Tôm hùng E',
                        trailing: '140',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      TopMonAnItem(
                        leading: '5',
                        title: 'Tôm hùng A',
                        trailing: '120',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                FlatButton(
                  onPressed: () {},
                  color: Colors.green,
                  minWidth: double.infinity,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Text('Xuất báo cáo'),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ],
    );
  }

  _listDate(int length) {
    final _widgets = <CustomSelectButton>[];
    final currentDate = DateTime.now();
    _widgets.add(CustomSelectButton(
      child: Text('Hôm nay'),
      value: 0,
    ));
    for (var i = 1; i <= length; i++) {
      final date = currentDate.subtract(Duration(days: i));
      _widgets.add(CustomSelectButton(
        child: Text(date.day.toString()),
        value: date.millisecondsSinceEpoch,
      ));
    }
    return _widgets.reversed.toList();
  }
}
