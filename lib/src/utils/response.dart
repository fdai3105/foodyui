import 'package:flutter/material.dart';

class Response extends StatelessWidget {
  final Widget small;
  final Widget medium;
  final Widget large;

  const Response({@required this.large, this.medium, this.small});

  // mobile
  static bool isSmallScreen(BuildContext context) {
    return MediaQuery.of(context).size.width < 800;
  }

  // table
  static bool isMediumScreen(BuildContext context) {
    return MediaQuery.of(context).size.width >= 800 &&
        MediaQuery.of(context).size.width <= 1200;
  }

  // desktop
  static bool isLargeScreen(BuildContext context) {
    return MediaQuery.of(context).size.width > 1200;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 800) {
          return small ?? large;
        } else if (constraints.maxWidth >= 800 &&
            constraints.maxWidth <= 1200) {
          return medium ?? large;
        } else {
          return large;
        }
      },
    );
  }
}
