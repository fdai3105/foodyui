import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:hues_foody/src/presentation/widgets/widgets.dart';
import 'package:hues_foody/src/resource/models/models.dart';
import 'package:hues_foody/src/utils/utils.dart';
import 'package:intl/intl.dart';

class KhachHangTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            color: Colors.white,
            child: Column(
              children: [
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                      '${DateFormat('H:m d/M/y').format(DateTime.now())}',
                      style: AppStyle.text),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Thống kê số lượng khách hàng', style: AppStyle.sub),
                    Container(
                      decoration: BoxDecoration(
                        color: AppColors.colorSecondary,
                        shape: BoxShape.circle,
                      ),
                      child: IconButton(
                        icon: Icon(Icons.settings),
                        onPressed: () {},
                        color: Colors.white,
                        padding: EdgeInsets.all(6),
                        constraints: BoxConstraints(),
                      ),
                    ),
                  ],
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  reverse: true,
                  child: CustomSelect(
                    buttons: _listDate(14),
                    onChanged: (value) {},
                    defaultSelect: 0,
                    selectDecoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.yellow.shade800,
                          width: 4,
                        ),
                      ),
                    ),
                    selectStyle: TextStyle(
                        color: Colors.yellow.shade800,
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                    textStyle: TextStyle(
                      color: Colors.black54,
                      fontSize: 18,
                    ),
                    paddingContent:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  ),
                ),
                SizedBox(height: 10),
              ],
            ),
          ),
          Container(
            color: AppColors.colorSecondary,
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CustomDropdown(
                  onChanged: (value) {},
                  items: [
                    DropdownMenuItem(
                      child: Text('Nhà hàng Hoa Mai 1- HN'),
                    ),
                  ],
                  hint: 'Chọn nhà hàng',
                ),
                SizedBox(height: 20),
                ArrowChartWidget(),
                SizedBox(height: 20),
                TwoFlexChartWidget(),
                SizedBox(height: 10),
                ChartBarExpansionWidget(
                  [
                    Bar(x: 0, y1: 1, y2: 2),
                    Bar(x: 1, y1: 2, y2: 1),
                    Bar(x: 2, y1: 1, y2: 3),
                    Bar(x: 3, y2: 3, y1: 2),
                    Bar(x: 4, y1: 1, y2: 5),
                    Bar(x: 5, y1: 2, y2: 1),
                    Bar(x: 6, y1: 2, y2: 4),
                  ],
                  y1Color: Colors.blueGrey,
                  title: 'Khách hàng mới tuần này',
                  subTitle: '50',
                  totalPercent: 50,
                  losePercent: 5,
                  isGrowUp: false,
                ),
                SizedBox(height: 10),
                ChartBarExpansionWidget(
                  [
                    Bar(x: 0, y1: 1, y2: 2),
                    Bar(x: 1, y1: 2, y2: 1),
                    Bar(x: 2, y1: 1, y2: 3),
                    Bar(x: 3, y2: 3, y1: 2),
                    Bar(x: 4, y1: 1, y2: 5),
                    Bar(x: 5, y1: 2, y2: 1),
                    Bar(x: 6, y1: 2, y2: 4),
                  ],
                  y1Color: Colors.red,
                  title: 'Khách hàng mới tuần này',
                  subTitle: '50',
                  totalPercent: 50,
                  losePercent: 5,
                  isGrowUp: false,
                ),
                SizedBox(height: 10),
                ChartBarExpansionWidget(
                  [
                    Bar(x: 0, y1: 1, y2: 2),
                    Bar(x: 1, y1: 2, y2: 1),
                    Bar(x: 2, y1: 1, y2: 3),
                    Bar(x: 3, y2: 3, y1: 2),
                    Bar(x: 4, y1: 1, y2: 5),
                    Bar(x: 5, y1: 2, y2: 1),
                    Bar(x: 6, y1: 2, y2: 4),
                  ],
                  y1Color: Colors.yellowAccent,
                  title: 'Khách hàng mới tuần này',
                  subTitle: '50',
                  totalPercent: 50,
                  losePercent: 5,
                  isGrowUp: false,
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Khách hàng chi tiêu nhiều nhất',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    DropdownButton(
                      items: [
                        DropdownMenuItem(
                          child: Text('Top 5'),
                          value: 0,
                        ),
                        DropdownMenuItem(
                          child: Text('Top 10'),
                          value: 1,
                        ),
                        DropdownMenuItem(
                          child: Text('Top 15'),
                          value: 2,
                        )
                      ],
                      value: 0,
                      onChanged: (value) {},
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                      underline: Container(),
                      iconEnabledColor: Colors.white,
                      dropdownColor: AppColors.colorSecondary,
                    ),
                  ],
                ),
                SizedBox(height: 10),
                MostSpendWidget(),
                SizedBox(height: 10),
                TwoPieChartWidget(
                  title: 'Thống kê khách hàng theo khu vực',
                  pies1: [
                    Pie(
                      value: 65,
                      title: '65%',
                      note: 'Toàn Quốc',
                      color: AppColors.colorSecondary,
                    ),
                    Pie(
                      value: 35,
                      title: '35%',
                      note: 'Toàn Quốc',
                      color: Colors.green,
                    ),
                  ],
                  pie1Title: 'Hôm nay',
                  pies2: [
                    Pie(
                      value: 65,
                      title: '65%',
                      note: 'Toàn Quốc',
                      color: AppColors.colorSecondary,
                    ),
                    Pie(
                      value: 5,
                      title: '5%',
                      note: 'Hà Nội',
                      color: Colors.green,
                    ),
                  ],
                  pie2Title: 'Hôm qua',
                ),
                SizedBox(height: 10),
                TwoPieChartWidget(
                  title: 'Thống kê khách hàng theo hạng',
                  pies1: [
                    Pie(
                      value: 50,
                      title: '50%',
                      note: 'VIP 1',
                      color: Colors.yellow,
                    ),
                    Pie(
                      value: 25,
                      title: '25%',
                      note: 'VIP 2',
                      color: Colors.green,
                    ),
                    Pie(
                      value: 25,
                      title: '25%',
                      note: 'VIP 3',
                      color: AppColors.colorSecondary,
                    ),
                  ],
                  pie1Title: 'Hôm nay',
                  pies2: [
                    Pie(
                      value: 45,
                      title: '45%',
                      note: 'VIP 1',
                      color: Colors.yellow,
                    ),
                    Pie(
                      value: 25,
                      title: '25%',
                      note: 'VIP 2',
                      color: Colors.green,
                    ),
                    Pie(
                      value: 30,
                      title: '30%',
                      note: 'VIP 3',
                      color: AppColors.colorSecondary,
                    ),
                  ],
                  pie2Title: 'Hôm qua',
                ),
                SizedBox(height: 10),
                CustomExpansion(
                  title: 'Thống kê theo độ tuổi',
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'ĐVT: khách hàng',
                      ),
                    ),
                    SizedBox(height: 20),
                    BarChartWeekly(
                      [
                        Bar(x: 0, y1: 40, y2: 35),
                        Bar(x: 1, y1: 75, y2: 65),
                        Bar(x: 2, y1: 24, y2: 28),
                        Bar(x: 3, y1: 12, y2: 25),
                        Bar(x: 4, y1: 34, y2: 23),
                      ],
                      bottomTitles: ['<18', '18-25', '25-35', '35-50', '>50'],
                      y1Color: Colors.green,
                      y1Title: 'Nam',
                      y2Title: 'Nữ',
                      showGrid: true,
                      gridStep: 25,
                      height: 200,
                    ),
                  ],
                ),
                SizedBox(height: 10),
                TwoPieChartWidget(
                  title: 'Thống kê giới tính',
                  pies1: [
                    Pie(
                      value: 65,
                      title: '65%',
                      note: 'Nam',
                      color: AppColors.colorSecondary,
                    ),
                    Pie(
                      value: 35,
                      title: '35%',
                      note: 'Nữ',
                      color: Colors.green,
                    ),
                  ],
                  pie1Title: 'Hôm nay',
                  pies2: [
                    Pie(
                      value: 65,
                      title: '95%',
                      note: 'Nam',
                      color: AppColors.colorSecondary,
                    ),
                    Pie(
                      value: 5,
                      title: '5%',
                      note: 'Nữ',
                      color: Colors.green,
                    ),
                  ],
                  pie2Title: 'Hôm qua',
                ),
                SizedBox(height: 20),
                FlatButton(
                  onPressed: () {},
                  color: AppColors.colorPrimary,
                  minWidth: double.infinity,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Text(
                    'Xuất Báo Cáo',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _listDate(int length) {
    final _widgets = <CustomSelectButton>[];
    final currentDate = DateTime.now();
    _widgets.add(CustomSelectButton(
      child: Text('Hôm nay'),
      value: 0,
    ));
    for (var i = 1; i <= length; i++) {
      final date = currentDate.subtract(Duration(days: i));
      _widgets.add(CustomSelectButton(
        child: Text(date.day.toString()),
        value: date.millisecondsSinceEpoch,
      ));
    }
    return _widgets.reversed.toList();
  }
}

class ArrowChartWidget extends StatelessWidget {
  const ArrowChartWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          children: [
            ArrowChartItemWidget(
              icon: Icon(
                Icons.person,
                color: Colors.white,
              ),
              title: '250',
              desc: 'Khách hàng',
            ),
            ArrowChartItemWidget(
              icon: Icon(
                Icons.person,
                color: Colors.transparent,
              ),
              title: '250',
              desc: 'Khách hàng',
            ),
            ArrowChartItemWidget(
              icon: Icon(
                Icons.person,
                color: Colors.transparent,
              ),
              title: '250',
              desc: 'Khách hàng',
            ),
          ],
        ),
        Column(
          children: [
            ArrowChartItemWidget(
              isGrow: true,
              title: '2%',
              desc: '1 tháng trước',
            ),
            ArrowChartItemWidget(
              isGrow: true,
              title: '2%',
              desc: '1 tháng trước',
            ),
            ArrowChartItemWidget(
              isGrow: true,
              title: '2%',
              desc: '1 tháng trước',
            ),
          ],
        ),
        Column(
          children: [
            ArrowChartItemWidget(
              isGrow: false,
              title: '2%',
              desc: '1 tháng trước',
            ),
            ArrowChartItemWidget(
              isGrow: false,
              title: '2%',
              desc: '2 tháng trước',
            ),
            ArrowChartItemWidget(
              isGrow: false,
              title: '2%',
              desc: '3 tháng trước',
            ),
          ],
        ),
      ],
    );
  }
}

class ChartBarExpansionWidget extends StatelessWidget {
  final List<Bar> bars;
  final Color y1Color;
  final Color y2Color;
  final String title;
  final String subTitle;
  final int totalPercent;
  final int losePercent;
  final bool isGrowUp;

  const ChartBarExpansionWidget(
    this.bars, {
    Key key,
    this.title,
    this.subTitle,
    this.totalPercent,
    this.losePercent,
    this.y1Color,
    this.y2Color,
    this.isGrowUp = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomExpansion(
      title: title ?? 'null',
      children: [
        Row(
          children: [
            Flexible(
              flex: 2,
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text('$subTitle' ?? '', style: AppStyle.sub),
                  ),
                  BarChartWeekly(
                    bars,
                    bottomTitles: ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'],
                    y1Color: y1Color,
                    y2Color: y2Color,
                    showDot: true,
                    y1Title: 'Tuần trước',
                    y2Title: 'Tuần trước',
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Icon(Icons.arrow_upward_rounded, color: Colors.white),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: '$totalPercent% \n', style: AppStyle.sub),
                            TextSpan(
                                text: 'Tổng số khách', style: AppStyle.text)
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 30),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      isGrowUp
                          ? Icon(Icons.arrow_downward_rounded,
                              color: Colors.red)
                          : Icon(Icons.arrow_upward_rounded,
                              color: Colors.green),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: '$losePercent% \n', style: AppStyle.sub),
                            TextSpan(text: 'Tuần trước', style: AppStyle.text)
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        )
      ],
    );
  }
}

class TwoFlexChartWidget extends StatelessWidget {
  const TwoFlexChartWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          flex: 1,
          child: CustomExpansion(
            title: 'Số lượng đặt hàng',
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text('20', style: AppStyle.sub),
              ),
              BarChartWeekly(
                [
                  Bar(x: 0, y1: 1, y2: 2),
                  Bar(x: 1, y1: 1, y2: 2),
                  Bar(x: 2, y1: 1, y2: 2),
                  Bar(x: 3, y1: 1, y2: 2),
                  Bar(x: 4, y1: 1, y2: 2),
                  Bar(x: 5, y1: 1, y2: 2),
                  Bar(x: 6, y1: 1, y2: 2),
                ],
                bottomTitles: ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'],
                y1Color: Colors.green,
                y1Title: 'Tuần này',
                y2Title: 'Tuần trước',
              ),
            ],
          ),
        ),
        SizedBox(width: 10),
        Flexible(
          flex: 1,
          child: CustomExpansion(
            title: 'Tỷ lệ lấp đầy bàn',
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text('', style: AppStyle.sub),
              ),
              BarChartWeekly(
                [
                  Bar(x: 0, y1: 1, y2: 2),
                  Bar(x: 1, y1: 1, y2: 2),
                  Bar(x: 2, y1: 1, y2: 2),
                  Bar(x: 3, y1: 1, y2: 2),
                  Bar(x: 4, y1: 1, y2: 2),
                  Bar(x: 5, y1: 1, y2: 2),
                  Bar(x: 6, y1: 1, y2: 2),
                ],
                bottomTitles: ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'],
                y1Color: Colors.yellow,
                y1Title: 'Tuần này',
                y2Title: 'Tuần trước',
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class MostSpendWidget extends StatelessWidget {
  const MostSpendWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: 10,
      physics: Response.isSmallScreen(context)
          ? BouncingScrollPhysics()
          : NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return MostSpendItem(
          index: index,
        );
      },
    );
  }
}

class TwoPieChartWidget extends StatelessWidget {
  final String title;
  final List<Pie> pies1;
  final String pie1Title;
  final List<Pie> pies2;
  final String pie2Title;

  TwoPieChartWidget({
    Key key,
    @required this.pies1,
    this.pie1Title,
    @required this.pies2,
    this.pie2Title,
    this.title,
  }) : super(key: key);

  _createDotList() {
    final _badges = <DotWidget>[];
    final _map = <Color, String>{};
    pies1.forEach((element) {
      if (!_map.containsValue(element.color)) {
        _map.addAll({
          element.color: element.note,
        });
      }
    });
    pies2.forEach((element) {
      if (!_map.containsValue(element.color)) {
        _map.addAll({
          element.color: element.note,
        });
      }
    });

    _map.forEach((key, value) {
      _badges.add(DotWidget(
        text: value,
        color: key,
      ));
    });
    return _badges;
  }

  @override
  Widget build(BuildContext context) {
    return CustomExpansion(
      title: title ?? 'null',
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                ChartPie(pies: pies1),
                SizedBox(height: 10),
                Text(pie1Title ?? 'null', style: AppStyle.sub),
              ],
            ),
            Column(
              children: [
                ChartPie(pies: pies2),
                SizedBox(height: 10),
                Text(pie2Title ?? 'null', style: AppStyle.sub),
              ],
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Divider(color: Colors.black87.withOpacity(0.4)),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: _createDotList(),
        )
      ],
    );
  }
}
