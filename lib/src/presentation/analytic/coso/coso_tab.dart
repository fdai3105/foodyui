import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:hues_foody/src/presentation/presentation.dart';
import 'package:intl/intl.dart';

class CoSoTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  '${DateFormat('H:m d/M/y').format(DateTime.now())}',
                  style: AppStyle.text,
                )),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'So sánh trong hệ thống',
                  style: AppStyle.title,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: AppColors.colorSecondary,
                    shape: BoxShape.circle,
                  ),
                  child: IconButton(
                    icon: Icon(Icons.settings),
                    onPressed: () {},
                    color: Colors.white,
                    padding: EdgeInsets.all(6),
                    constraints: BoxConstraints(),
                  ),
                )
              ],
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            reverse: true,
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
            child: CustomSelect(
              buttons: _calendar(10),
              onChanged: (value) {},
              defaultSelect: 0,
              selectDecoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.yellow.shade800,
                    width: 4,
                  ),
                ),
              ),
              selectStyle: TextStyle(
                  color: Colors.yellow.shade800,
                  fontSize: 18,
                  fontWeight: FontWeight.w600),
              textStyle: TextStyle(
                color: Colors.black54,
                fontSize: 18,
              ),
              space: 40,
              paddingContent: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
            ),
          ),
          Expanded(
            child: DefaultTabController(
              length: 3,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomTabBar(
                    tabs: [
                      Tab(text: 'Khách hàng'),
                      Tab(text: 'Món ăn'),
                      Tab(text: 'Doanh thu'),
                    ],
                  ),
                  Flexible(
                    fit: FlexFit.loose,
                    child: TabBarView(
                      children: [
                        CSKhachHangTab(),
                        CSMonAnTab(),
                        CSDoanhThuTab(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _calendar(int weeks) {
    final _list = <CustomSelectButton>[]..add(
        CustomSelectButton(
          child: Text('Tuần này'),
          value: 0,
        ),
      );
    var currentDate = DateTime.now();
    for (int i = 0; i < weeks; i++) {
      final endDate = currentDate.subtract(
        Duration(days: currentDate.weekday),
      );
      final startDate = endDate.subtract(
        Duration(days: DateTime.daysPerWeek),
      );
      currentDate = endDate;
      if (startDate.month == endDate.month) {
        _list.add(
          CustomSelectButton(
            child: Text(
                '${DateFormat('d').format(startDate)}-${DateFormat('d/M').format(endDate)}'),
            value: '${startDate.day}.${endDate.day}',
          ),
        );
      } else {
        _list.add(CustomSelectButton(
          child: Text(
              '${DateFormat('d/M').format(startDate)} - ${DateFormat('d/M').format(endDate)}'),
          value: '${startDate.day}.${endDate.day}',
        ));
      }
    }

    return _list.reversed.toList();
  }
}
