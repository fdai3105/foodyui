import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:hues_foody/src/presentation/presentation.dart';
import 'package:hues_foody/src/resource/models/models.dart';
import 'package:hues_foody/src/utils/utils.dart';

class MALamLaiTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Response.isSmallScreen(context) ? _mobile(context) : _table(context);
  }

  _mobile(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 10),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.symmetric(horizontal: 20),
            reverse: true,
            child: CustomSelect(
              buttons: _listDate(14),
              onChanged: (value) {},
              defaultSelect: 0,
              selectDecoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.yellow.shade800,
                    width: 4,
                  ),
                ),
              ),
              selectStyle: TextStyle(
                  color: Colors.yellow.shade800,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
              textStyle: TextStyle(
                  color: Colors.black54,
                  fontSize: 14,
                  fontWeight: FontWeight.w600),
              space: 40,
              paddingContent: EdgeInsets.symmetric(horizontal: 6, vertical: 10),
            ),
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: CustomDropdown(
              items: [
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 1 - Hà Nội'),
                  value: 'hanoi',
                ),
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 2 - Huế'),
                  value: 'hue',
                )
              ],
              onChanged: (value) {},
              hint: 'Chọn nhà hàng',
              shadow: true,
            ),
          ),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color: AppColors.colorSecondary,
            child: Container(
              child: Column(
                children: [
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SortButtonWidget(
                        onChanged: (value) {},
                      ),
                      DropdownButtonHideUnderline(
                        child: DropdownButton(
                          items: [
                            DropdownMenuItem(
                              child: Text('Top 5'),
                              value: 5,
                            ),
                            DropdownMenuItem(
                              child: Text('Top 10'),
                              value: 10,
                            ),
                            DropdownMenuItem(
                              child: Text('Top 15'),
                              value: 15,
                            ),
                          ],
                          onChanged: (value) {},
                          value: 5,
                          isDense: true,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                          dropdownColor: AppColors.colorSecondary,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Column(
                    children: [
                      TopMonAnItem(
                        isFirst: true,
                        leading: '1',
                        title: 'Tôm hùng A',
                        trailing: '25',
                        trailingSub: 'Lượt',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      TopMonAnItem(
                        leading: '2',
                        title: 'Tôm hùng C',
                        trailing: '180',
                        trailingSub: 'Lượt',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      TopMonAnItem(
                        leading: '3',
                        title: 'Tôm hùng D',
                        trailing: '160',
                        trailingSub: 'Lượt',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      TopMonAnItem(
                        leading: '4',
                        title: 'Tôm hùng E',
                        trailing: '140',
                        trailingSub: 'Lượt',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      TopMonAnItem(
                        leading: '5',
                        title: 'Tôm hùng A',
                        trailing: '120',
                        trailingSub: 'Lượt4',
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: CustomExpansion(
                              title: 'Thống kê theo ngày',
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          children: [
                                            DotWidget(
                                              color: Colors.green,
                                              text: 'Hôm Nay',
                                            ),
                                            SizedBox(height: 20),
                                            DotWidget(
                                              text: 'Hôm Qua',
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: BarChartWeekly(
                                          [
                                            Bar(x: 0, y1: 200, y2: 100),
                                          ],
                                          showDot: false,
                                          showGrid: true,
                                          gridStep: 100,
                                          unit: 'Lượt',
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  FlatButton(
                    onPressed: () {},
                    color: Colors.green,
                    minWidth: double.infinity,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Text('Xuất báo cáo'),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _table(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.symmetric(horizontal: 20),
          reverse: true,
          child: CustomSelect(
            buttons: _listDate(14),
            onChanged: (value) {},
            defaultSelect: 0,
            selectDecoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.yellow.shade800,
                  width: 4,
                ),
              ),
            ),
            selectStyle: TextStyle(
                color: Colors.yellow.shade800,
                fontSize: 16,
                fontWeight: FontWeight.w600),
            textStyle: TextStyle(
                color: Colors.black54,
                fontSize: 14,
                fontWeight: FontWeight.w600),
            space: 40,
            paddingContent: EdgeInsets.symmetric(horizontal: 6, vertical: 10),
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: CustomDropdown(
            items: [
              DropdownMenuItem(
                child: Text('Nhà hàng Hoa Mai 1 - Hà Nội'),
                value: 'hanoi',
              ),
              DropdownMenuItem(
                child: Text('Nhà hàng Hoa Mai 2 - Huế'),
                value: 'hue',
              )
            ],
            onChanged: (value) {},
            hint: 'Chọn nhà hàng',
            shadow: true,
          ),
        ),
        SizedBox(height: 20),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color: AppColors.colorSecondary,
            child: Container(
              child: Column(
                children: [
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SortButtonWidget(
                        onChanged: (value) {},
                      ),
                      DropdownButtonHideUnderline(
                        child: DropdownButton(
                          items: [
                            DropdownMenuItem(
                              child: Text('Top 5'),
                              value: 5,
                            ),
                            DropdownMenuItem(
                              child: Text('Top 10'),
                              value: 10,
                            ),
                            DropdownMenuItem(
                              child: Text('Top 15'),
                              value: 15,
                            ),
                          ],
                          onChanged: (value) {},
                          value: 5,
                          isDense: true,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                          dropdownColor: AppColors.colorSecondary,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Expanded(
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        TopMonAnItem(
                          isFirst: true,
                          leading: '1',
                          title: 'Tôm hùng A',
                          trailing: '25',
                          trailingSub: 'Lượt',
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: CustomExpansion(
                                title: 'Thống kê theo ngày',
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      children: [
                                        Flexible(
                                          flex: 1,
                                          child: Column(
                                            children: [
                                              DotWidget(
                                                color: Colors.green,
                                                text: 'Hôm Nay',
                                              ),
                                              SizedBox(height: 20),
                                              DotWidget(
                                                text: 'Hôm Qua',
                                              ),
                                            ],
                                          ),
                                        ),
                                        Flexible(
                                          flex: 1,
                                          child: BarChartWeekly(
                                            [
                                              Bar(x: 0, y1: 200, y2: 100),
                                            ],
                                            showDot: false,
                                            showGrid: true,
                                            gridStep: 100,
                                            unit: 'Lượt',
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        TopMonAnItem(
                          leading: '2',
                          title: 'Tôm hùng C',
                          trailing: '180',
                          trailingSub: 'Lượt',
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: CustomExpansion(
                                title: 'Thống kê theo ngày',
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      children: [
                                        Flexible(
                                          flex: 1,
                                          child: Column(
                                            children: [
                                              DotWidget(
                                                color: Colors.green,
                                                text: 'Hôm Nay',
                                              ),
                                              SizedBox(height: 20),
                                              DotWidget(
                                                text: 'Hôm Qua',
                                              ),
                                            ],
                                          ),
                                        ),
                                        Flexible(
                                          flex: 1,
                                          child: BarChartWeekly(
                                            [
                                              Bar(x: 0, y1: 200, y2: 100),
                                            ],
                                            showDot: false,
                                            showGrid: true,
                                            gridStep: 100,
                                            unit: 'Lượt',
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        TopMonAnItem(
                          leading: '3',
                          title: 'Tôm hùng D',
                          trailing: '160',
                          trailingSub: 'Lượt',
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: CustomExpansion(
                                title: 'Thống kê theo ngày',
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      children: [
                                        Flexible(
                                          flex: 1,
                                          child: Column(
                                            children: [
                                              DotWidget(
                                                color: Colors.green,
                                                text: 'Hôm Nay',
                                              ),
                                              SizedBox(height: 20),
                                              DotWidget(
                                                text: 'Hôm Qua',
                                              ),
                                            ],
                                          ),
                                        ),
                                        Flexible(
                                          flex: 1,
                                          child: BarChartWeekly(
                                            [
                                              Bar(x: 0, y1: 200, y2: 100),
                                            ],
                                            showDot: false,
                                            showGrid: true,
                                            gridStep: 100,
                                            unit: 'Lượt',
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        TopMonAnItem(
                          leading: '4',
                          title: 'Tôm hùng E',
                          trailing: '140',
                          trailingSub: 'Lượt',
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: CustomExpansion(
                                title: 'Thống kê theo ngày',
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      children: [
                                        Flexible(
                                          flex: 1,
                                          child: Column(
                                            children: [
                                              DotWidget(
                                                color: Colors.green,
                                                text: 'Hôm Nay',
                                              ),
                                              SizedBox(height: 20),
                                              DotWidget(
                                                text: 'Hôm Qua',
                                              ),
                                            ],
                                          ),
                                        ),
                                        Flexible(
                                          flex: 1,
                                          child: BarChartWeekly(
                                            [
                                              Bar(x: 0, y1: 200, y2: 100),
                                            ],
                                            showDot: false,
                                            showGrid: true,
                                            gridStep: 100,
                                            unit: 'Lượt',
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        TopMonAnItem(
                          leading: '5',
                          title: 'Tôm hùng A',
                          trailing: '120',
                          trailingSub: 'Lượt4',
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: CustomExpansion(
                                title: 'Thống kê theo ngày',
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      children: [
                                        Flexible(
                                          flex: 1,
                                          child: Column(
                                            children: [
                                              DotWidget(
                                                color: Colors.green,
                                                text: 'Hôm Nay',
                                              ),
                                              SizedBox(height: 20),
                                              DotWidget(
                                                text: 'Hôm Qua',
                                              ),
                                            ],
                                          ),
                                        ),
                                        Flexible(
                                          flex: 1,
                                          child: BarChartWeekly(
                                            [
                                              Bar(x: 0, y1: 200, y2: 100),
                                            ],
                                            showDot: false,
                                            showGrid: true,
                                            gridStep: 100,
                                            unit: 'Lượt',
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  FlatButton(
                    onPressed: () {},
                    color: Colors.green,
                    minWidth: double.infinity,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Text('Xuất báo cáo'),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  _listDate(int months) {
    final _list = <CustomSelectButton>[]..add(
        CustomSelectButton(
          child: Text('Tháng này'),
          value: 0,
        ),
      );
    var currentDate = DateTime.now();
    for (int i = 0; i < months; i++) {
      currentDate =
          DateTime(currentDate.year, currentDate.month - 1, currentDate.day);
      if (currentDate.year == DateTime.now().year) {
        _list.add(CustomSelectButton(
          child: Text('Tháng ${currentDate.month}'),
          value: '${currentDate.month}.${currentDate.year}',
        ));
      } else {
        _list.add(CustomSelectButton(
          child: Text('Tháng ${currentDate.month} - ${currentDate.year}'),
          value: '${currentDate.month}.${currentDate.year}',
        ));
      }
    }
    return _list.reversed.toList();
  }
}
