import 'package:flutter/material.dart';
import '../presentation.dart';

class AnalyticScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: DefaultTabController(
          length: 4,
          initialIndex: 0,
          child: Column(
            children: [
              CustomAppbar(
                tabs: [
                  Tab(text: 'Khách hàng'),
                  Tab(text: 'Món ăn'),
                  Tab(text: 'Doanh thu'),
                  Tab(text: 'Cơ sở'),
                ],
                leading: IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: Icon(Icons.arrow_back_ios_rounded),
                  color: Colors.white,
                ),
              ),
              Expanded(
                child: TabBarView(
                  children: [
                    KhachHangTab(),
                    MonAnTab(),
                    DoanhThuTab(),
                    CoSoTab(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
