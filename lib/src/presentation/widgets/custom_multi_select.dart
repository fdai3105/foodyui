import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TableMultiSelect extends StatefulWidget {
  final List<CustomMultiSelectButton> buttons;
  final Function(dynamic) onChanged;
  final Function(dynamic) onLongTap;
  final TextStyle textStyle;
  final TextStyle selectStyle;
  final BoxDecoration decoration;
  final BoxDecoration selectDecoration;
  final EdgeInsets paddingContent;
  final double space;
  final MainAxisAlignment mainAxisAlignment;
  final bool scrollable;

  TableMultiSelect(
      {Key key,
      @required this.buttons,
      @required this.onChanged,
      this.textStyle,
      this.decoration,
      this.paddingContent,
      this.selectStyle,
      this.space = 0,
      this.mainAxisAlignment,
      this.selectDecoration,
      this.onLongTap,
      this.scrollable = true})
      : super(key: key);

  @override
  _TableMultiSelectState createState() => _TableMultiSelectState();
}

class _TableMultiSelectState extends State<TableMultiSelect> {
  List<bool> _selects;

  @override
  void initState() {
    _selects = List.generate(widget.buttons.length, (index) => false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GridView(
      shrinkWrap: true,
      physics: widget.scrollable
          ? BouncingScrollPhysics()
          : NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        mainAxisSpacing: 10,
      ),
      children: _button(),
    );
  }

  _button() {
    final widgets = <Widget>[];
    for (var i = 0; i < widget.buttons.length; i++) {
      widgets.add(GestureDetector(
        onTap: () {
          setState(() {
            _selects[i] = !_selects[i];
          });
          final _list = <dynamic>[];
          for (int i = 0; i < _selects.length; i++) {
            if (_selects[i]) {
              _list.add(widget.buttons[i].value);
            }
          }
          widget.onChanged(_list);
        },
        onLongPress: () {
          widget.onLongTap(widget.buttons[i].value);
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 6),
              padding: EdgeInsets.all(4),
              decoration: _selects[i]
                  ? widget.selectDecoration ??
                      BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(10),
                      )
                  : widget.decoration ?? BoxDecoration(),
              child: DefaultTextStyle(
                style: _selects[i]
                    ? widget.selectStyle ?? TextStyle(color: Colors.black)
                    : widget.textStyle ?? TextStyle(color: Colors.black),
                child: widget.buttons[i],
              ),
            ),
            _selects[i]
                ? Positioned(
                    top: 0,
                    right: 0,
                    child: Container(
                        padding: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                            color: Colors.green, shape: BoxShape.circle),
                        child: Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 14,
                        )),
                  )
                : Container(),
          ],
        ),
      ));
    }
    return widgets;
  }
}

class CustomMultiSelectButton extends StatelessWidget {
  final Widget child;
  final value;

  const CustomMultiSelectButton({Key key, this.child, this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child;
  }
}
