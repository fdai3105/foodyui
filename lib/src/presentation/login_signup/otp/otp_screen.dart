import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hues_foody/src/presentation/widgets/widgets.dart';
import 'package:hues_foody/src/utils/utils.dart';
import 'package:pinput/pin_put/pin_put.dart';

class OTPScreen extends StatelessWidget {
  final OTP isForget;

  const OTPScreen({Key key, this.isForget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Image.asset(
                  'assets/images/component-9-13@3x.png',
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    Header(),
                    OTPForm(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class OTPForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _pinController = TextEditingController();
    return FormContainer(
      child: Column(
        children: [
          Text('Nhập mã OTP'),
          SizedBox(height: 20),
          Text(
            'Xin mời bạn nhập mã OTP \n'
            'chúng tôi đã gửi đến số ...',
          ),
          SizedBox(height: 30),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: PinPut(
              controller: _pinController,
              onChanged: (value) {
                if (value.length == 4) {
                  OTP args = ModalRoute.of(context).settings.arguments;
                  Navigator.pushReplacementNamed(
                    context,
                    args.isForget ? Routes.changePassScreen : Routes.roleScreen,
                  );
                }
              },
              fieldsCount: 4,
              keyboardType: TextInputType.number,
              textStyle: TextStyle(
                color: Colors.green,
                fontSize: 24,
                fontWeight: FontWeight.w600,
              ),
              autovalidateMode: AutovalidateMode.onUserInteraction,
              eachFieldHeight: 60,
              eachFieldWidth: 50,
              pinAnimationType: PinAnimationType.fade,
              followingFieldDecoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(0, 0),
                      blurRadius: 6,
                    )
                  ]),
              selectedFieldDecoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.green, width: 2),
              ),
              submittedFieldDecoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          SizedBox(height: 20),
          OTPButton(onTap: () {})
        ],
      ),
    );
  }
}

class OTPButton extends StatefulWidget {
  final Function onTap;

  const OTPButton({Key key, this.onTap}) : super(key: key);

  @override
  _OTPButtonState createState() => _OTPButtonState();
}

class _OTPButtonState extends State<OTPButton> {
  var _countDown;
  Timer _timer;
  bool countDone;

  @override
  void initState() {
    _start();
    super.initState();
  }

  void _start() {
    _countDown = 10;
    countDone = false;
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _countDown--;
      });

      if (_countDown == 0) {
        if (_timer.isActive) {
          _timer.cancel();
        }
        setState(() {
          countDone = !countDone;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FlatButton(
          onPressed: countDone
              ? () {
                  widget.onTap();
                  _start();
                }
              : null,
          disabledTextColor: Colors.grey,
          textColor: Colors.green,
          child: Text(
            'Gửi lại mã OTP',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        SizedBox(height: 10),
        Text(
          'OPT có hiệu lực trong 00: ${_countDown < 10 ? '0$_countDown' : _countDown}',
          style: TextStyle(
            color: Colors.red,
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}