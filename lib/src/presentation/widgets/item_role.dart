import 'package:flutter/material.dart';

class RoleItem extends StatelessWidget {
  final Function onTap;
  final avatar;
  final role;

  const RoleItem({
    Key key,
    @required this.onTap,
    @required this.avatar,
    @required this.role,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          avatar != null ? Image.asset(avatar, width: 80) : Container(),
          SizedBox(
            height: 10,
          ),
          Text(
            role ?? 'null',
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
              fontWeight: FontWeight.w600,
            ),
          )
        ],
      ),
    );
  }
}
