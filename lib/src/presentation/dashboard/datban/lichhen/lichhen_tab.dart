import 'package:flutter/material.dart';
import 'package:hues_foody/src/presentation/presentation.dart';

class LichHenTab extends StatelessWidget {
  const LichHenTab({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          child: Image.asset(
            'assets/images/component-9-13@3x.png',
            fit: BoxFit.fitWidth,
          ),
        ),
        Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Image.asset('assets/images/group-2336.png'),
                      Text('  Đã tới giờ hẹn'),
                    ],
                  ),
                  Row(
                    children: [
                      Image.asset('assets/images/group-2337.png'),
                      Text('  Sắp tới'),
                    ],
                  ),
                  Row(
                    children: [
                      Image.asset('assets/images/group-2338.png'),
                      Text('  Đã huỷ'),
                    ],
                  )
                ],
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.vertical(top: Radius.circular(20))
                ),
                child: ListView.builder(
                  physics: BouncingScrollPhysics(),
                  shrinkWrap: true,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  itemCount: 10,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: ScheduleItem(),
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  fakeData() {
    final _list = [];
    return _list;
  }
}
