import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 30),
      color: Colors.green,
      child: Image.asset(
        'assets/images/a-lab@3x.png',
        width: 120,
        height: 120,
      ),
    );
  }
}
