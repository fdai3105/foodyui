import 'package:flutter/material.dart';
import 'package:hues_foody/src/presentation/widgets/widgets.dart';
import 'package:hues_foody/src/utils/utils.dart';
import 'package:provider/provider.dart';

import 'dashboard.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>
    with TickerProviderStateMixin {
  TabController tabCtrl;

  @override
  void initState() {
    tabCtrl = TabController(length: 4, vsync: this, initialIndex: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => _checkPop(),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
          child: Column(
            children: [
              CustomAppbar(
                tabControl: tabCtrl,
                tabs: [
                  Tab(text: 'Đặt bàn'),
                  Tab(text: 'Gọi món'),
                  Tab(text: 'Phục Vụ'),
                  Tab(text: 'Thanh Toán'),
                ],
                actions: [
                  IconButton(
                    icon: Icon(
                      Icons.list,
                      color: Colors.white,
                    ),
                    onPressed: () => Navigator.pushNamed(
                      context,
                      Routes.analyticScreen,
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Container(
                  color: Colors.white,
                  child: TabBarView(
                    controller: tabCtrl,
                    children: [
                      DatBanTab(),
                      GoiMonTab(),
                      PhucVuTab(),
                      ThanhToanTab(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _checkPop() {
    if (tabCtrl.index == 0) {
      return Provider.of<DatBanViewModel>(context, listen: false).checkPop();
    } else {
      setState(() {
        tabCtrl.index--;
      });
      return false;
    }
  }
}
