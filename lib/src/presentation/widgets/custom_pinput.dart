import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';

class CustomPinPut extends StatelessWidget {
  final TextEditingController controller;
  final Function(String) submit;

  const CustomPinPut({
    Key key,
    @required this.controller,
    @required this.submit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: PinPut(
        controller: controller,
        onChanged: (value) {
          if (value.length == 4) {
            submit(value);
          }
        },
        fieldsCount: 4,
        keyboardType: TextInputType.number,
        textStyle: TextStyle(
          color: Colors.green,
          fontSize: 24,
          fontWeight: FontWeight.w600,
        ),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        eachFieldHeight: 60,
        eachFieldWidth: 50,
        pinAnimationType: PinAnimationType.fade,
        followingFieldDecoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                offset: Offset(0, 0),
                blurRadius: 6,
              )
            ]),
        selectedFieldDecoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.green, width: 2),
        ),
        submittedFieldDecoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}