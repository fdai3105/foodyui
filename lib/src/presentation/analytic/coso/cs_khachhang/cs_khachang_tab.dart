import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:hues_foody/src/presentation/presentation.dart';
import 'package:hues_foody/src/resource/models/models.dart';
import 'package:hues_foody/src/resource/models/pie.dart';
import 'package:hues_foody/src/utils/utils.dart';

class CSKhachHangTab extends StatelessWidget {
  const CSKhachHangTab({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Response.isSmallScreen(context)
        ? _mobile(context)
        : _table(context);
  }

  _mobile(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        color: AppColors.colorSecondary,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            CustomDropdown(
              hint: 'Nhà hàng',
              items: [
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 1 - Hà Nội'),
                  value: 'hanoi',
                ),
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 2 - Huế'),
                  value: 'hue',
                )
              ],
              onChanged: (value) {},
              shadow: true,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                ArrowChartItemWidget(
                  icon: Icon(Icons.person,color: Colors.white),
                  title: '250',
                  desc: 'Khách đặt bàn',
                ),
                ArrowChartItemWidget(
                  title: '5%',
                  desc: 'Hôm qua',
                  isGrow: true,
                ),
                ArrowChartItemWidget(
                  title: '2%',
                  desc: '1 tháng trước',
                  isGrow: false,
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  flex: 1,
                  child: CustomExpansion(
                    title: 'Quận Ba Đình',
                    children: [
                      ChartPie(
                        pies: [
                          Pie(
                              color: AppColors.colorPrimary,
                              value: 65,
                              title: '65%'),
                          Pie(
                              color: AppColors.colorSecondary,
                              value: 35,
                              title: '35%'),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(width: 10),
                Flexible(
                  flex: 1,
                  child: CustomExpansion(
                    title: 'Hà Nội',
                    children: [
                      ChartPie(
                        pies: [
                          Pie(
                              color: AppColors.colorSecondary,
                              value: 65,
                              title: '65%'),
                          Pie(
                              color: AppColors.colorPrimary,
                              value: 35,
                              title: '35%'),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            CustomExpansion(
              title: 'Toàn quốc',
              children: [
                ChartPie(
                  pies: [
                    Pie(
                        color: AppColors.colorSecondary,
                        value: 95,
                        title: '95%'),
                    Pie(color: Colors.red, value: 5, title: '5%'),
                  ],
                ),
              ],
            ),
            SizedBox(height: 10),
            CustomExpansion(
              title: 'So sánh nhà hàng trong cùng quận',
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Text('ĐVT: Lượt khách'),
                ),
                SizedBox(height: 10),
                BarChartWeekly(
                  [
                    Bar(x: 0, y1: 120, y2: 80),
                    Bar(x: 1, y1: 190, y2: 150),
                    Bar(x: 2, y1: 400, y2: 70),
                    Bar(x: 3, y1: 190, y2: 120),
                    Bar(x: 4, y1: 120, y2: 80),
                    Bar(x: 5, y1: 210, y2: 120),
                  ],
                  bottomTitles: [
                    'Cơ sở 1',
                    'Cơ sở 2',
                    'Cơ sở 3',
                    'Cơ sở 4',
                    'Cơ sở 5',
                    'Cơ sở 6',
                  ],
                  y1Color: Colors.green,
                  y1Title: 'Tuần này',
                  y2Title: 'Tuần trước',
                  showGrid: true,
                  gridStep: 100,
                  divider: true,
                  unit: 'Lượt khách',
                  height: 240,
                )
              ],
            ),
            SizedBox(height: 10),
            CustomExpansion(
              title: 'So sánh giữa các quận',
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Text('ĐVT: Lượt khách'),
                ),
                SizedBox(height: 10),
                BarChartWeekly(
                  [
                    Bar(x: 0, y1: 120, y2: 80),
                    Bar(x: 1, y1: 190, y2: 150),
                    Bar(x: 2, y1: 400, y2: 70),
                    Bar(x: 3, y1: 190, y2: 120),
                  ],
                  bottomTitles: [
                    'Quận A',
                    'Quận B',
                    'Quận C',
                    'Quận D',
                  ],
                  y1Color: Colors.green,
                  y1Title: 'Tuần này',
                  y2Title: 'Tuần trước',
                  showGrid: true,
                  gridStep: 100,
                  divider: true,
                  unit: 'Lượt khách',
                  height: 240,
                )
              ],
            ),
            SizedBox(height: 10),
            CustomExpansion(
              title: 'So sánh giữa các thành phố',
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Text('ĐVT: Lượt khách'),
                ),
                SizedBox(height: 10),
                BarChartWeekly(
                  [
                    Bar(x: 0, y1: 120, y2: 80),
                    Bar(x: 1, y1: 190, y2: 150),
                    Bar(x: 2, y1: 400, y2: 70),
                    Bar(x: 3, y1: 190, y2: 120),
                  ],
                  bottomTitles: [
                    'Hà Nội',
                    'Đà Nẵng',
                    'Hồ Chí Minh',
                    'Cần Thơ',
                  ],
                  y1Color: Colors.green,
                  y1Title: 'Tuần này',
                  y2Title: 'Tuần trước',
                  showGrid: true,
                  gridStep: 100,
                  divider: true,
                  unit: 'Lượt khách',
                  height: 240,
                )
              ],
            ),
            SizedBox(height: 20),
            FlatButton(
              onPressed: () {},
              color: Colors.green,
              minWidth: double.infinity,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Text(
                'Xuất báo cáo',
                style: TextStyle(color: Colors.white),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );

  }

  _table(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      color: AppColors.colorSecondary,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            CustomDropdown(
              hint: 'Nhà hàng',
              items: [
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 1 - Hà Nội'),
                  value: 'hanoi',
                ),
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 2 - Huế'),
                  value: 'hue',
                )
              ],
              onChanged: (value) {},
              shadow: true,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                ArrowChartItemWidget(
                  icon: Icon(Icons.person,color: Colors.white),
                  title: '250',
                  desc: 'Khách đặt bàn',
                ),
                ArrowChartItemWidget(
                  title: '5%',
                  desc: 'Hôm qua',
                  isGrow: true,
                ),
                ArrowChartItemWidget(
                  title: '2%',
                  desc: '1 tháng trước',
                  isGrow: false,
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  flex: 1,
                  child: CustomExpansion(
                    title: 'Quận Ba Đình',
                    children: [
                      ChartPie(
                        pies: [
                          Pie(
                              color: AppColors.colorPrimary,
                              value: 65,
                              title: '65%'),
                          Pie(
                              color: AppColors.colorSecondary,
                              value: 35,
                              title: '35%'),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(width: 10),
                Flexible(
                  flex: 1,
                  child: CustomExpansion(
                    title: 'Hà Nội',
                    children: [
                      ChartPie(
                        pies: [
                          Pie(
                              color: AppColors.colorSecondary,
                              value: 65,
                              title: '65%'),
                          Pie(
                              color: AppColors.colorPrimary,
                              value: 35,
                              title: '35%'),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            CustomExpansion(
              title: 'Toàn quốc',
              children: [
                ChartPie(
                  pies: [
                    Pie(
                        color: AppColors.colorSecondary,
                        value: 95,
                        title: '95%'),
                    Pie(color: Colors.red, value: 5, title: '5%'),
                  ],
                ),
              ],
            ),
            SizedBox(height: 10),
            CustomExpansion(
              title: 'So sánh nhà hàng trong cùng quận',
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Text('ĐVT: Lượt khách'),
                ),
                SizedBox(height: 10),
                BarChartWeekly(
                  [
                    Bar(x: 0, y1: 120, y2: 80),
                    Bar(x: 1, y1: 190, y2: 150),
                    Bar(x: 2, y1: 400, y2: 70),
                    Bar(x: 3, y1: 190, y2: 120),
                    Bar(x: 4, y1: 120, y2: 80),
                    Bar(x: 5, y1: 210, y2: 120),
                  ],
                  bottomTitles: [
                    'Cơ sở 1',
                    'Cơ sở 2',
                    'Cơ sở 3',
                    'Cơ sở 4',
                    'Cơ sở 5',
                    'Cơ sở 6',
                  ],
                  y1Color: Colors.green,
                  y1Title: 'Tuần này',
                  y2Title: 'Tuần trước',
                  showGrid: true,
                  gridStep: 100,
                  divider: true,
                  unit: 'Lượt khách',
                  height: 240,
                )
              ],
            ),
            SizedBox(height: 10),
            CustomExpansion(
              title: 'So sánh giữa các quận',
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Text('ĐVT: Lượt khách'),
                ),
                SizedBox(height: 10),
                BarChartWeekly(
                  [
                    Bar(x: 0, y1: 120, y2: 80),
                    Bar(x: 1, y1: 190, y2: 150),
                    Bar(x: 2, y1: 400, y2: 70),
                    Bar(x: 3, y1: 190, y2: 120),
                  ],
                  bottomTitles: [
                    'Quận A',
                    'Quận B',
                    'Quận C',
                    'Quận D',
                  ],
                  y1Color: Colors.green,
                  y1Title: 'Tuần này',
                  y2Title: 'Tuần trước',
                  showGrid: true,
                  gridStep: 100,
                  divider: true,
                  unit: 'Lượt khách',
                  height: 240,
                )
              ],
            ),
            SizedBox(height: 10),
            CustomExpansion(
              title: 'So sánh giữa các thành phố',
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Text('ĐVT: Lượt khách'),
                ),
                SizedBox(height: 10),
                BarChartWeekly(
                  [
                    Bar(x: 0, y1: 120, y2: 80),
                    Bar(x: 1, y1: 190, y2: 150),
                    Bar(x: 2, y1: 400, y2: 70),
                    Bar(x: 3, y1: 190, y2: 120),
                  ],
                  bottomTitles: [
                    'Hà Nội',
                    'Đà Nẵng',
                    'Hồ Chí Minh',
                    'Cần Thơ',
                  ],
                  y1Color: Colors.green,
                  y1Title: 'Tuần này',
                  y2Title: 'Tuần trước',
                  showGrid: true,
                  gridStep: 100,
                  divider: true,
                  unit: 'Lượt khách',
                  height: 240,
                )
              ],
            ),
            SizedBox(height: 20),
            FlatButton(
              onPressed: () {},
              color: Colors.green,
              minWidth: double.infinity,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Text(
                'Xuất báo cáo',
                style: TextStyle(color: Colors.white),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
