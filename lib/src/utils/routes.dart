import 'package:flutter/material.dart';
import 'package:hues_foody/src/presentation/presentation.dart';

class Routes {
  static const String loginScreen = 'login_screen';
  static const String signupScreen = 'signup_screen';
  static const String otpScreen = 'otp_screen';
  static const String changePassScreen = 'change_pass_screen';
  static const String forgetPassScreen = 'forget_pass_screen';
  static const String roleScreen = 'role_screen';
  static const String dashboardScreen = 'dashboard_screen';
  static const String analyticScreen = 'analytic_screen';

  static final Map<String, WidgetBuilder> routes = {
    loginScreen: (_) => LoginScreen(),
    signupScreen: (_) => SignupScreen(),
    otpScreen: (_) => OTPScreen(),
    changePassScreen: (_) => ChangePasswordScreen(),
    forgetPassScreen: (_) => ForgetPasswordScreen(),
    roleScreen: (_) => RoleScreen(),
    dashboardScreen: (_) => DashboardScreen(),
    analyticScreen: (_) => AnalyticScreen(),
  };
}

class OTP {
  final bool isForget;

  OTP({@required this.isForget});
}
