import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';

class CustomAppbar extends StatelessWidget {
  final tabControl;
  final List<Tab> tabs;
  final Function(int) onTap;
  final Widget leading;
  final List<Widget> actions;

  const CustomAppbar({
    Key key,
    this.tabControl,
    @required this.tabs,
    this.onTap,
    this.leading,
    this.actions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 60,
              color: AppColors.colorPrimary,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  leading ?? Container(),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: actions ?? [Container()],
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 30, bottom: 20),
              decoration: BoxDecoration(
                color: AppColors.colorSecondary,
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(10),
                ),
              ),
              child: TabBar(
                controller: tabControl,
                tabs: tabs,
                onTap: onTap,
                labelColor: Colors.yellow,
                unselectedLabelColor: Colors.white54,
                labelStyle: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
                unselectedLabelStyle: TextStyle(fontSize: 14),
                indicatorSize: TabBarIndicatorSize.label,
                indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(
                      color: Colors.yellow, width: 4, style: BorderStyle.solid),
                ),
                labelPadding: EdgeInsets.zero,
              ),
            ),
          ],
        ),
        Positioned(
            top: 30,
            left: 0,
            right: 0,
            child: Image.asset(
              'assets/images/group-6@2x.png',
              height: 60,
              width: 60,
            )),
      ],
    );
  }
}
