import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/app_style.dart';
import 'package:hues_foody/src/presentation/presentation.dart';
import 'package:provider/provider.dart';

class TaoLichTab1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DatBanViewModel>(context, listen: false);
    return Column(
      children: [
        SizedBox(height: 10),
        SelectDateTime(),
        SizedBox(height: 20),
        Expanded(
          child: Stack(
            children: [
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Image.asset(
                  'assets/images/component-9-13@3x.png',
                  fit: BoxFit.fitWidth,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: [
                    Text(
                      'Chọn vị trí',
                      style: AppStyle.title,
                    ),
                    SizedBox(height: 10),
                    CustomInput(
                      hint: 'Số lượng khách',
                      isPassword: false,
                      shadow: true,
                    ),
                    CustomDropdown(
                      items: [
                        DropdownMenuItem(
                          child: Text('Khu 1'),
                          value: 1,
                        ),
                        DropdownMenuItem(
                          child: Text('Khu 2'),
                          value: 2,
                        ),
                      ],
                      hint: 'Khu vực',
                      shadow: true,
                      onChanged: (value) {},
                    ),
                    CustomDropdown(
                      items: [
                        DropdownMenuItem(
                          child: Text('Tầng 1'),
                          value: 1,
                        ),
                        DropdownMenuItem(
                          child: Text('Tầng 2'),
                          value: 2,
                        ),
                      ],
                      hint: 'Tầng',
                      shadow: true,
                      onChanged: (value) {},
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 20,
                left: 30,
                right: 30,
                child: FlatButton(
                  onPressed: () => provider.nextPage(),
                  color: Colors.green,
                  child: Text(
                    'Tiếp theo',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class SelectDateTime extends StatefulWidget {
  @override
  _SelectDateTimeState createState() => _SelectDateTimeState();
}

class _SelectDateTimeState extends State<SelectDateTime> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Chọn ngày',
          style: AppStyle.title,
        ),
        SizedBox(height: 10),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: CustomSelect(
            buttons: _listDate(),
            onChanged: (value) {},
            defaultSelect: 0,
            paddingContent: EdgeInsets.symmetric(
              horizontal: 12,
              vertical: 12,
            ),
            space: 10,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.black12, width: 1),
            ),
            selectDecoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.circular(10),
            ),
            textStyle: TextStyle(
              color: Colors.black87,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
            selectStyle: TextStyle(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(height: 20),
        Text(
          'Chọn giờ',
          style: AppStyle.title,
        ),
        SizedBox(height: 10),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: CustomSelect(
            buttons: [
              CustomSelectButton(
                child: Text('08:00'),
                value: 9,
              ),
              CustomSelectButton(
                child: Text('09:00'),
                value: 10,
              ),
              CustomSelectButton(
                child: Text('10:00'),
                value: 10,
              ),
              CustomSelectButton(
                child: Text('11:00'),
                value: 11,
              ),
            ],
            onChanged: (value) {},
            paddingContent: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 6,
            ),
            space: 10,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Colors.black12, width: 1),
            ),
            selectDecoration: BoxDecoration(
              color: Colors.yellow,
              borderRadius: BorderRadius.circular(20),
            ),
            textStyle: TextStyle(
                color: Colors.black87,
                fontSize: 16,
                fontWeight: FontWeight.w500),
            selectStyle: TextStyle(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),
            mainAxisAlignment: MainAxisAlignment.center,
          ),
        ),
      ],
    );
  }

  _listDate() {
    final buttons = <CustomSelectButton>[];
    var now = DateTime.now();
    for (var i = 0; i < 7; i++) {
      final current = now.add(Duration(days: i));
      buttons.add(
        CustomSelectButton(
          child: Column(
            children: [
              Text('${current.day}'),
              SizedBox(height: 6),
              Text(
                now.day == current.day ? 'Hôm Nay' : 'Thứ ${current.weekday}',
                style: TextStyle(fontWeight: FontWeight.w400),
              ),
            ],
          ),
          value: i,
        ),
      );
    }
    return buttons;
  }
}
