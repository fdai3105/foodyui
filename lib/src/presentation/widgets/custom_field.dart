import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class CustomInput extends StatefulWidget {
  final String hint;
  final bool isPassword;
  final bool shadow;
  final FormFieldValidator validator;
  final TextEditingController controller;
  final MaskTextInputFormatter mask;
  final TextInputType type;
  final EdgeInsets padding;

  const CustomInput({
    Key key,
    @required this.hint,
    this.isPassword = false,
    this.validator,
    this.controller,
    this.mask,
    this.type,
    this.shadow = false, this.padding,
  }) : super(key: key);

  @override
  _CustomInputState createState() => _CustomInputState();
}

class _CustomInputState extends State<CustomInput> {
  var isShow;

  @override
  void initState() {
    isShow = widget.isPassword ? true : false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding ?? const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: widget.shadow
              ? [
                  BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0, 0),
                      spreadRadius: 0,
                      blurRadius: 6)
                ]
              : [],
        ),
        child: Row(
          children: [
            Flexible(
              child: TextFormField(
                controller: widget.controller,
                validator: widget.validator,
                inputFormatters: widget.mask != null ? [widget.mask] : null,
                keyboardType: widget.type ?? TextInputType.text,
                obscureText: isShow,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  border: InputBorder.none,
                  hintText: widget.hint ?? 'null',
                ),
              ),
            ),
            widget.isPassword
                ? IconButton(
                    onPressed: () {
                      setState(() {
                        isShow = !isShow;
                      });
                    },
                    icon: Icon(
                      isShow ? CupertinoIcons.eye_slash : CupertinoIcons.eye,
                    ),
                    highlightColor: Colors.transparent,
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
