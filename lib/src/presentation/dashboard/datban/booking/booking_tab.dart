import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:hues_foody/src/presentation/presentation.dart';

class BookingTab extends StatelessWidget {
  const BookingTab({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  Image.asset('assets/images/group-2336.png'),
                  Text('  Còn trống'),
                ],
              ),
              Row(
                children: [
                  Image.asset('assets/images/group-2337.png'),
                  Text('  Đã đặt'),
                ],
              ),
              Row(
                children: [
                  Image.asset('assets/images/group-2338.png'),
                  Text('  Khách đã tới'),
                ],
              )
            ],
          ),
        ),
        Expanded(
          child: Container(
            color: AppColors.colorSecondary,
            child: GridView(
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3, crossAxisSpacing: 30, mainAxisSpacing: 10),
              children: _fakeData(),
            ),
          ),
        ),
      ],
    );
  }

  _fakeData() {
    final list = [
      TableWidget(chairsSlot: 4, status: TableStatus.BOOKED, desc: 'Bàn 1'),
      TableWidget(chairsSlot: 4, status: TableStatus.BOOKED, desc: 'Bàn 2'),
      TableWidget(chairsSlot: 4, status: TableStatus.EMPTY, desc: 'Bàn 3'),
      TableWidget(
        chairsSlot: 6,
        status: TableStatus.EMPTY,
        booked: 2,
        desc: 'Bàn 4',
      ),
      TableWidget(chairsSlot: 10, status: TableStatus.EMPTY, desc: 'Bàn 5'),
      TableWidget(chairsSlot: 2, status: TableStatus.CAME, desc: 'Bàn 6'),
    ];
    return list;
  }
}
