import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:hues_foody/src/presentation/widgets/custom_tabbar.dart';
import 'package:intl/intl.dart';

import '../../presentation.dart';

class MonAnTab extends StatelessWidget {
  const MonAnTab({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  '${DateFormat('H:m d/M/y').format(DateTime.now())}',
                  style: AppStyle.text,
                )),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Thống kê món ăn',
                  style: AppStyle.title,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: AppColors.colorSecondary,
                    shape: BoxShape.circle,
                  ),
                  child: IconButton(
                    icon: Icon(Icons.settings),
                    onPressed: () {},
                    color: Colors.white,
                    padding: EdgeInsets.all(6),
                    constraints: BoxConstraints(),
                  ),
                ),
              ],
            ),
          ),
          MonAnSubBody(),
        ],
      ),
    );
  }
}

class MonAnSubBody extends StatelessWidget {
  const MonAnSubBody({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: DefaultTabController(
        length: 3,
        initialIndex: 0,
        child: Column(
          children: [
            CustomTabBar(
              tabs: [
                Tab(text: 'Lượt đặt'),
                Tab(text: 'Doanh thu'),
                Tab(text: 'Làm lại'),
              ],
              padding: EdgeInsets.zero,
            ),
            Expanded(
              child: TabBarView(
                children: [
                  MALuotDatTab(),
                  MADoanhThuTab(),
                  MALamLaiTab(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
