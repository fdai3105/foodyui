import 'package:flutter/material.dart';

class AppStyle {
  static final TextStyle title = TextStyle(
      color: Colors.black.withOpacity(0.80),
      fontSize: 22,
      fontWeight: FontWeight.w600);
  static final TextStyle titleWhite = TextStyle(
      color: Colors.white.withOpacity(0.80),
      fontSize: 22,
      fontWeight: FontWeight.w600);

  static final TextStyle sub = TextStyle(
      color: Colors.black.withOpacity(0.80),
      fontSize: 18,
      fontWeight: FontWeight.w600);
  static final TextStyle subWhite = TextStyle(
      color: Colors.white.withOpacity(0.80),
      fontSize: 18,
      fontWeight: FontWeight.w600);

  static final TextStyle text = TextStyle(
      color: Colors.black.withOpacity(0.70), fontWeight: FontWeight.w500);
  static final TextStyle textWhite = TextStyle(
      color: Colors.white.withOpacity(0.70), fontWeight: FontWeight.w500);

  static final TextStyle tab = TextStyle(
    color: Colors.black54,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  );
  static final TextStyle tabSelected = TextStyle(
    color: Colors.red,
    fontSize: 18,
    fontWeight: FontWeight.w600,
  );
}
