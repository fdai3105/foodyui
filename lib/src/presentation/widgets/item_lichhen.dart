import 'package:flutter/material.dart';
import 'package:hues_foody/src/presentation/presentation.dart';

class ScheduleItem extends StatelessWidget {
  const ScheduleItem({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (_) {
              return CustomDialog(
                widget: Column(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () => Navigator.pop(context),
                        color: Colors.white,
                      ),
                    ),
                    Text('Mã đặt chỗ: xxx'),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Text('1'),
                            Text('Bàn số'),
                          ],
                        ),
                        Column(
                          children: [
                            Text('1'),
                            Text('Khu vực'),
                          ],
                        ),
                        Column(
                          children: [
                            Text('3'),
                            Text('Tổng'),
                          ],
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Divider(
                        color: Colors.white,
                        thickness: 1,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Anh abc'),
                        Text('0777230926'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Khách hàng'),
                        Text('Số điện thoại'),
                      ],
                    ),
                    SizedBox(
                        height: 140
                    ),
                    Column(
                      children: [
                        FlatButton(
                          onPressed: () {},
                          minWidth: double.infinity,
                          color: Colors.green,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Text('Khách đã đến'),
                        ),
                        FlatButton(
                          onPressed: () {},
                          minWidth: double.infinity,
                          color: Colors.blue,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Text('Khách đã đến'),
                        ),
                        FlatButton(
                          onPressed: () {},
                          minWidth: double.infinity,
                          color: Colors.red,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Text('Khách đã đến'),
                        ),
                      ],
                    )
                  ],
                ),
              );
            });
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.circular(100),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.all(18),
              decoration: BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(Icons.clear),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('MĐC : 000007'),
                        SizedBox(height: 6),
                        Text('Anh Dũng'),
                      ],
                    ),
                    Text('0777230926'),
                  ],
                ),
              ),
            ),
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.horizontal(
                  right: Radius.circular(10),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.access_time_outlined,
                    color: Colors.white,
                  ),
                  SizedBox(height: 6),
                  Text(
                    '9:00',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}