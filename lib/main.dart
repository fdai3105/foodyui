import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hues_foody/src/presentation/presentation.dart';
import 'package:hues_foody/src/utils/utils.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.green,
  ));
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(
      create: (_) => DatBanViewModel()..init(),
    ),
  ],child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: LoginScreen(),
      routes: Routes.routes,
    );
  }
}
