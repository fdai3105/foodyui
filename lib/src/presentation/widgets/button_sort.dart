import 'package:flutter/material.dart';

enum SortType { highToLow, lowToHigh }

class SortButtonWidget extends StatefulWidget {
  final Function(SortType) onChanged;

  const SortButtonWidget({Key key, @required this.onChanged}) : super(key: key);

  @override
  _SortButtonWidgetState createState() => _SortButtonWidgetState();
}

class _SortButtonWidgetState extends State<SortButtonWidget> {
  SortType currentSort;

  @override
  void initState() {
    currentSort = SortType.highToLow;
    widget.onChanged(currentSort);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              currentSort = SortType.highToLow;
            });
            widget.onChanged(currentSort);
          },
          child: Container(
            height: 40,
            width: 120,
            decoration: BoxDecoration(
              color: currentSort == SortType.highToLow
                  ? Colors.green
                  : Colors.white,
              borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(Icons.sort_rounded, color: Colors.black54),
                Text('Cao nhất'),
              ],
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              currentSort = SortType.lowToHigh;
            });
            widget.onChanged(currentSort);
          },
          child: Container(
            height: 40,
            width: 120,
            decoration: BoxDecoration(
              color: currentSort == SortType.lowToHigh
                  ? Colors.green
                  : Colors.white,
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(Icons.sort_rounded, color: Colors.black54),
                Text('Thấp nhất'),
              ],
            ),
          ),
        ),
      ],
    );
  }
}