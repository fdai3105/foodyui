import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';

class CustomExpansion extends StatelessWidget {
  final String title;
  final List<Widget> children;

  const CustomExpansion({Key key, this.children, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          title: Text(
            title ?? '',
            style: AppStyle.text.copyWith(fontWeight: FontWeight.w600),
          ),
          tilePadding: EdgeInsets.zero,
          childrenPadding: EdgeInsets.only(bottom: 10),
          children: children ?? [],
          initiallyExpanded: true,
        ),
      ),
    );
  }
}
