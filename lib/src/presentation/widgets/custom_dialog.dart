import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';

class CustomDialog extends StatelessWidget {
  final Widget widget;
  final String title;

  const CustomDialog({
    Key key,
    this.widget,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.colorSecondary.withOpacity(0.7),
      textStyle: TextStyle(color: Colors.white, fontSize: 16),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: IconButton(
                icon: Icon(Icons.clear),
                onPressed: () => Navigator.pop(context),
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    title ?? 'null',
                    style: AppStyle.title.copyWith(color: Colors.white),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  widget,
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
