import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hues_foody/src/presentation/widgets/custom_dropdown.dart';

class PhoneCodeSelect extends StatefulWidget {
  final ValueChanged valueChanged;
  final EdgeInsets padding;

  const PhoneCodeSelect({Key key, @required this.valueChanged, this.padding})
      : super(key: key);

  @override
  _PhoneCodeSelectState createState() => _PhoneCodeSelectState();
}

class _PhoneCodeSelectState extends State<PhoneCodeSelect> {
  var _currentCode;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding ??
          const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: FutureBuilder(
          future: _list(context),
          builder: (context, builder) {
            if (builder.hasData) {
              return CustomDropdown(
                items: builder.data,
                value: _currentCode,
                onChanged: (value) {
                  widget.valueChanged(value);
                  setState(() {
                    _currentCode = value;
                  });
                },
                hint: 'Zip code',
              );
            } else {
              return const Padding(
                padding: EdgeInsets.all(6.0),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Future<List<DropdownMenuItem>> _list(BuildContext context) async {
    final items = <DropdownMenuItem>[];
    final json = await DefaultAssetBundle.of(context)
        .loadString('assets/jsons/country_codes.json');
    List.from(jsonDecode(json)).forEach((element) {
      items.add(
        DropdownMenuItem(
          child: Text('${element['name']} (${element['dial_code']})'),
          onTap: () {},
          value: element['code'] + element['dial_code'],
        ),
      );
    });
    return items;
  }
}
