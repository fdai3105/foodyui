import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:hues_foody/src/presentation/presentation.dart';
import 'package:hues_foody/src/utils/utils.dart';
import 'package:provider/provider.dart';

class TaoLichTab2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Response.isMediumScreen(context)
        ? _tablet(context)
        : _mobile(context);
  }

  _mobile(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '15/05',
                        style: AppStyle.title,
                      ),
                      Text('Ngày hẹn'),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      Text(
                        '09:00',
                        style: AppStyle.title,
                      ),
                      Text('Giờ hẹn'),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '10',
                        style: AppStyle.title,
                      ),
                      Text('Chỗ'),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Text(
            'Chọn bàn',
            style: AppStyle.title,
          ),
          SizedBox(height: 10),
          CustomDropdown(
            hint: 'Khu',
            items: [
              DropdownMenuItem(
                child: Text('Khu 1'),
                value: 1,
              ),
              DropdownMenuItem(
                child: Text('Khu 2'),
                value: 2,
              ),
            ],
            onChanged: (value) {},
            value: 1,
            shadow: true,
          ),
          CustomDropdown(
            hint: 'Tầng',
            items: [
              DropdownMenuItem(
                child: Text('Tầng 1'),
                value: 1,
              ),
              DropdownMenuItem(
                child: Text('Tầng 2'),
                value: 2,
              ),
            ],
            onChanged: (value) {},
            value: 2,
            shadow: true,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              DotWidget(color: Colors.green, text: 'Còn trống'),
              DotWidget(color: Colors.yellow, text: 'Đã đặt'),
              DotWidget(color: Colors.red, text: 'Khách đã tới'),
            ],
          ),
          Container(
            color: AppColors.colorSecondary,
            child: Column(
              children: [
                SizedBox(height: 10),
                Text(
                  'Khu 1 - Tầng 2',
                  style: AppStyle.title.copyWith(color: Colors.white),
                ),
                SizedBox(height: 6),
                TableMultiSelect(
                  buttons: [
                    CustomMultiSelectButton(
                      child: TableWidget(
                        chairsSlot: 4,
                        status: TableStatus.EMPTY,
                      ),
                      value: 'table0',
                    ),
                    CustomMultiSelectButton(
                      child: TableWidget(
                        chairsSlot: 4,
                        status: TableStatus.EMPTY,
                      ),
                      value: 'table1',
                    ),
                    CustomMultiSelectButton(
                      child: TableWidget(
                        chairsSlot: 4,
                        status: TableStatus.EMPTY,
                      ),
                      value: 'table2',
                    ),
                    CustomMultiSelectButton(
                      child: TableWidget(
                        chairsSlot: 4,
                        status: TableStatus.EMPTY,
                      ),
                      value: 'table3',
                    ),
                    CustomMultiSelectButton(
                      child: TableWidget(
                        chairsSlot: 4,
                        status: TableStatus.EMPTY,
                      ),
                      value: 'table4',
                    ),
                  ],
                  onChanged: (value) {
                    print(value);
                  },
                  onLongTap: (value) => _chooseChairDialog(context),
                  selectDecoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                SwitchListTile(
                  value: true,
                  onChanged: (value) {},
                  title: Text(
                    'Chọn cả khu',
                    style: TextStyle(color: Colors.white),
                  ),
                  activeColor: AppColors.colorPrimary,
                ),
                SwitchListTile(
                  value: false,
                  onChanged: (value) {},
                  title: Text(
                    'Chọn cả tầng',
                    style: TextStyle(color: Colors.white),
                  ),
                  activeColor: AppColors.colorPrimary,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                  child: FlatButton(
                    onPressed: () => _inputInfoDialog(context),
                    minWidth: double.infinity,
                    color: AppColors.colorPrimary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Text(
                      'Nhập thông tin',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  _tablet(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '15/05',
                      style: AppStyle.title,
                    ),
                    Text('Ngày hẹn'),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    Text(
                      '09:00',
                      style: AppStyle.title,
                    ),
                    Text('Giờ hẹn'),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      '10',
                      style: AppStyle.title,
                    ),
                    Text('Chỗ'),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Text(
          'Chọn bàn',
          style: AppStyle.title,
        ),
        SizedBox(height: 10),
        CustomDropdown(
          hint: 'Khu',
          items: [
            DropdownMenuItem(
              child: Text('Khu 1'),
              value: 1,
            ),
            DropdownMenuItem(
              child: Text('Khu 2'),
              value: 2,
            ),
          ],
          onChanged: (value) {},
          value: 1,
          shadow: true,
        ),
        CustomDropdown(
          hint: 'Tầng',
          items: [
            DropdownMenuItem(
              child: Text('Tầng 1'),
              value: 1,
            ),
            DropdownMenuItem(
              child: Text('Tầng 2'),
              value: 2,
            ),
          ],
          onChanged: (value) {},
          value: 2,
          shadow: true,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            DotWidget(color: Colors.green, text: 'Còn trống'),
            DotWidget(color: Colors.yellow, text: 'Đã đặt'),
            DotWidget(color: Colors.red, text: 'Khách đã tới'),
          ],
        ),
        Expanded(
          child: Container(
            color: AppColors.colorSecondary,
            child: Column(
              children: [
                SizedBox(height: 10),
                Text(
                  'Khu 1 - Tầng 2',
                  style: AppStyle.title.copyWith(color: Colors.white),
                ),
                SizedBox(height: 6),
                Expanded(
                  child: TableMultiSelect(
                    buttons: [
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table0',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table1',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table2',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table3',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table4',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table0',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table1',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table2',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table3',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table4',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table0',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table1',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table2',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table3',
                      ),
                      CustomMultiSelectButton(
                        child: TableWidget(
                          chairsSlot: 4,
                          status: TableStatus.EMPTY,
                        ),
                        value: 'table4',
                      ),
                    ],
                    onChanged: (value) {
                      print(value);
                    },
                    onLongTap: (value) => _chooseChairDialog(context),
                    selectDecoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                SwitchListTile(
                  value: true,
                  onChanged: (value) {},
                  title: Text(
                    'Chọn cả khu',
                    style: TextStyle(color: Colors.white),
                  ),
                  activeColor: AppColors.colorPrimary,
                ),
                SwitchListTile(
                  value: false,
                  onChanged: (value) {},
                  title: Text(
                    'Chọn cả tầng',
                    style: TextStyle(color: Colors.white),
                  ),
                  activeColor: AppColors.colorPrimary,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                  child: FlatButton(
                    onPressed: () => _inputInfoDialog(context),
                    minWidth: double.infinity,
                    color: AppColors.colorPrimary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Text(
                      'Nhập thông tin',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  // thong tin khach hang
  _inputInfoDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (_) {
          return CustomDialog(
            title: 'Thông tin khách hàng',
            widget: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomInput(
                  hint: 'Họ và tên',
                ),
                CustomInput(
                  hint: 'Họ và tên',
                ),
                CustomInput(
                  hint: 'Họ và tên',
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    children: [
                      Flexible(
                        flex: 1,
                        child: CustomInput(
                          hint: 'Họ',
                          padding: EdgeInsets.zero,
                        ),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        flex: 1,
                        child: CustomInput(
                          hint: 'tên',
                          padding: EdgeInsets.zero,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                SwitchListTile(
                  value: true,
                  onChanged: (value) {},
                  title: Text(
                    'Gửi SMS thông báo',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  activeColor: Colors.green,
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Provider.of<DatBanViewModel>(context, listen: false)
                        .nextPage();
                  },
                  color: Colors.green,
                  minWidth: double.infinity,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                      20,
                    ),
                  ),
                  child: Text(
                    'Xong',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          );
        });
  }

  // chon. ghe
  _chooseChairDialog(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (_) {
        return CustomDialog(
          title: 'Chọn ghế',
          widget: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('10',
                          style: AppStyle.sub.copyWith(
                            color: Colors.white,
                          )),
                      Text('Số ghế khách đặt'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text('8',
                          style: AppStyle.sub.copyWith(
                            color: Colors.white,
                          )),
                      Text('Số ghế đã chọn'),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 30),
              SelectChair(
                chairsSlot: 4,
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: FlatButton(
                  onPressed: () => Navigator.pop(context),
                  minWidth: double.infinity,
                  color: Colors.yellow,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Text(
                    'Xong',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class SelectChair extends StatefulWidget {
  final int chairsSlot;

  const SelectChair({Key key, this.chairsSlot}) : super(key: key);

  @override
  _SelectChairState createState() => _SelectChairState();
}

class _SelectChairState extends State<SelectChair> {
  List<bool> _chairSelect;
  bool selectAll;

  @override
  void initState() {
    _chairSelect = List.generate(widget.chairsSlot, (index) => false);
    selectAll = false;
    super.initState();
  }

  List<Widget> _stacks() {
    final list = <Widget>[];
    list.add(Align(
      alignment: Alignment(0, 0),
      child: Container(
        height: 140,
        width: 140,
        decoration: BoxDecoration(
          color: Colors.green,
          shape: BoxShape.circle,
        ),
      ),
    ));
    for (var i = 0; i < widget.chairsSlot; i++) {
      var degree = 360 / widget.chairsSlot * i;
      var radian = degree * (pi / 180);
      list.add(
        GestureDetector(
          onTap: () {
            setState(() {
              _chairSelect[i] = !_chairSelect[i];
            });
          },
          child: Align(
            alignment: Alignment(sin(radian), cos(radian)),
            child: Transform.rotate(
              angle: -radian,
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: _chairSelect[i] ? Colors.green : Colors.transparent,
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  'assets/images/chair.png',
                  height: 60,
                  color: _chairSelect[i] ? Colors.white : Colors.green,
                ),
              ),
            ),
          ),
        ),
      );
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 340,
          width: 340,
          child: Stack(
            children: _stacks(),
          ),
        ),
        SwitchListTile(
          value: selectAll,
          title: Text(
            'Chọn tất cả ghế',
            style: TextStyle(color: Colors.white),
          ),
          activeColor: Colors.green,
          onChanged: (value) {
            setState(() {
              selectAll = !selectAll;
              if (value) {
                _chairSelect =
                    List.generate(widget.chairsSlot, (index) => true);
              } else {
                _chairSelect =
                    List.generate(widget.chairsSlot, (index) => false);
              }
            });
          },
        )
      ],
    );
  }
}
