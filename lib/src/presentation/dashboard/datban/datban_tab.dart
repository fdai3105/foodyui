import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:provider/provider.dart';

import '../../presentation.dart';

class DatBanTab extends StatelessWidget {
  const DatBanTab({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DatBanViewModel>(context, listen: false);
    return PageView(
      controller: provider.pageController,
      physics: const NeverScrollableScrollPhysics(),
      children: [
        DatBanBody(provider: provider),
        TaoLichTab1(),
        TaoLichTab2(),
        TaoLichTab3(),
      ],
    );
  }
}

class DatBanBody extends StatelessWidget {
  const DatBanBody({
    Key key,
    @required this.provider,
  }) : super(key: key);

  final DatBanViewModel provider;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Column(
          children: [
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: FlatButton(
                onPressed: () => provider.nextPage(),
                minWidth: double.infinity,
                color: AppColors.colorPrimary,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text('Tạo lịch hẹn mới',
                    style: TextStyle(color: Colors.white)),
              ),
            ),
            SizedBox(height: 20),
            CustomInput(
              hint: 'Nhập mã đặt chỗ',
              isPassword: false,
              shadow: true,
            ),
            CustomDropdown(
              hint: 'Chọn khu',
              shadow: true,
              items: [
                DropdownMenuItem(
                  child: Text('Khu 1'),
                  value: '1',
                ),
                DropdownMenuItem(
                  child: Text('Khu 2'),
                  value: '2',
                ),
                DropdownMenuItem(
                  child: Text('Khu 3'),
                  value: '3',
                ),
              ],
              onChanged: (value) {},
            ),
            CustomDropdown(
              items: [
                DropdownMenuItem(
                  child: Text('Tầng 1'),
                  value: '1',
                ),
                DropdownMenuItem(
                  child: Text('Tầng 2'),
                  value: '2',
                ),
                DropdownMenuItem(
                  child: Text('Tầng 3'),
                  value: '3',
                ),
              ],
              hint: 'Chọn tầng',
              shadow: true,
              padding: EdgeInsets.only(left: 20,right: 20,bottom: 0),
              onChanged: (value) {},
            ),
          ],
        ),
        DatBanSubBody()
      ],
    );
  }
}

class DatBanSubBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            CustomTabBar(
              tabs: [
                Tab(text: 'Tình trạng booking'),
                Tab(text: 'Lịch hẹn'),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  BookingTab(),
                  LichHenTab(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}