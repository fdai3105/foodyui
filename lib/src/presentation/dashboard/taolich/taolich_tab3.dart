import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/config.dart';
import 'package:provider/provider.dart';

import '../../presentation.dart';

class TaoLichTab3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10),
        Text('Lịch hẹn đã đặt', style: AppStyle.title),
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('15/05', style: AppStyle.sub),
                        Text('Ngày hẹn', style: AppStyle.text),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('09:00', style: AppStyle.sub),
                        Text('Giờ hẹn', style: AppStyle.text),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text('10', style: AppStyle.sub),
                        Text('Chỗ', style: AppStyle.text),
                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Divider(
                  height: 1,
                  color: Colors.black,
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('15/05', style: AppStyle.sub),
                        Text('Ngày hẹn', style: AppStyle.text),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('09:00', style: AppStyle.sub),
                        Text('Giờ hẹn', style: AppStyle.text),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text('10', style: AppStyle.sub),
                        Text('Chỗ', style: AppStyle.text),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Expanded(
          child: Container(
            padding: EdgeInsets.all(20),
            color: AppColors.colorSecondary,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Text(
                      'Thông tin khách hàng',
                      style: AppStyle.titleWhite,
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Anh Dũng',
                              style: AppStyle.subWhite,
                            ),
                            Text('Họ và tên', style: AppStyle.textWhite)
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              '20202',
                              style: AppStyle.subWhite,
                            ),
                            Text('Mã đặt chỗ', style: AppStyle.textWhite)
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('0777230926', style: AppStyle.subWhite),
                            Text('Số điện thoại', style: AppStyle.textWhite)
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text('10', style: AppStyle.subWhite),
                            Text('Số lượng khách sẽ đến',
                                style: AppStyle.textWhite)
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Trống', style: AppStyle.subWhite),
                            Text('Sinh nhật', style: AppStyle.textWhite)
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text('Đã gửi', style: AppStyle.subWhite),
                            Text('SMS', style: AppStyle.textWhite)
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                FlatButton(
                  onPressed: () {
                    Provider.of<DatBanViewModel>(context, listen: false)
                        .backToHome();
                  },
                  minWidth: double.infinity,
                  color: Colors.green,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Text('Quay lại màn hình chính'),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}
