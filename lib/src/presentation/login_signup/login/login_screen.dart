import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/app_style.dart';
import 'package:hues_foody/src/presentation/widgets/widgets.dart';
import 'package:hues_foody/src/utils/utils.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset(
                'assets/images/component-9-13@3x.png',
              ),
            ),
            LoginForm(
              onLogin: () => Navigator.pushReplacementNamed(context, Routes.dashboardScreen),
            ),
            Positioned(
              top: 470,
              left: 0,
              right: 0,
              child: Image.asset(
                'assets/images/group-69@3x.png',
                width: 80,
                height: 80,
              ),
            ),
            OtherOption(),
          ],
        ),
      ),
    );
  }
}

class LoginForm extends StatelessWidget {
  final Function onLogin;

  const LoginForm({
    Key key,
    this.onLogin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Column(
        children: [
          Header(),
          FormContainer(
            child: Column(
              children: [
                Text(
                  'Đăng nhập',
                  style: AppStyle.title,
                ),
                SizedBox(height: 10),
                CustomInput(
                  hint: 'Tài khoản/Số điện thoại',
                  isPassword: false,
                ),
                CustomInput(
                  hint: 'Mật khẩu',
                  isPassword: true,
                  padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Checkbox(
                          value: true,
                          onChanged: (value) {},
                        ),
                        Text('Lưu đăng nhập'),
                      ],
                    ),
                    FlatButton(
                      onPressed: () =>
                          Navigator.pushNamed(context, Routes.forgetPassScreen),
                      child: Text('Quên mật khẩu?'),
                      padding: EdgeInsets.zero,
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                    )
                  ],
                ),
                FlatButton(
                  onPressed: onLogin,
                  color: Colors.green,
                  minWidth: double.infinity,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Text(
                    'Đăng nhập',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(height: 40),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class OtherOption extends StatelessWidget {
  const OtherOption({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20),
        margin: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.85),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(
              20,
            ),
            topRight: Radius.circular(
              20,
            ),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Text('Đăng nhập bằng tài khoản liên kết'),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () {},
                  icon: Image.asset(
                    'assets/images/group-2331@3x.png',
                  ),
                  iconSize: 60,
                ),
                SizedBox(
                  width: 14,
                ),
                IconButton(
                  onPressed: () {},
                  icon: Image.asset('assets/images/group-2332@3x.png'),
                  iconSize: 60,
                ),
                SizedBox(
                  width: 14,
                ),
                IconButton(
                  onPressed: () {},
                  icon: Image.asset('assets/images/group-2333@3x.png'),
                  iconSize: 60,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Chưa có tài khoản? '),
                GestureDetector(
                  onTap: () =>
                      Navigator.pushNamed(context, Routes.signupScreen),
                  child: Text(
                    'Đăng ký ngay',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
