import 'package:flutter/material.dart';
import 'package:hues_foody/src/presentation/presentation.dart';
import 'package:hues_foody/src/utils/utils.dart';

class RoleScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.green,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Chọn vai trò',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 28,
                  fontWeight: FontWeight.w600),
            ),
            SizedBox(height: 80),
            Column(
              children: [
                RoleItem(
                  onTap: () => Navigator.pushReplacementNamed(
                      context, Routes.dashboardScreen),
                  avatar: 'assets/images/group-6@2x.png',
                  role: 'Lễ tân',
                ),
                SizedBox(height: 30),
                RoleItem(
                  onTap: () {},
                  avatar: 'assets/images/group-6@2x.png',
                  role: 'Đầu bếp',
                ),
                SizedBox(height: 30),
                RoleItem(
                  onTap: () {},
                  avatar: 'assets/images/group-6@2x.png',
                  role: 'Admin',
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}