import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/app_style.dart';
import 'package:hues_foody/src/presentation/widgets/widgets.dart';
import 'package:hues_foody/src/utils/utils.dart';

class ChangePasswordScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset(
                'assets/images/component-9-13@3x.png',
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Column(
                children: [Header(), ChangePassForm()],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ChangePassForm extends StatelessWidget {
  const ChangePassForm({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormContainer(
      child: Column(
        children: [
          Text('Thay đổi mật khẩu', style: AppStyle.title),
          SizedBox(height: 10),
          Text(
              'Vui lòng xác nhận mật khẩu mới và xác nhận \n'
              'mật khẩu mới vào ô bên dưới',
              textAlign: TextAlign.center,
              style: AppStyle.text),
          SizedBox(height: 20),
          CustomInput(hint: 'Mật khẩu mới', isPassword: true),
          CustomInput(hint: 'Nhập lại mật khẩu mới', isPassword: true),
          FlatButton(
            onPressed: () =>
                Navigator.pushNamed(context, Routes.dashboardScreen),
            minWidth: double.infinity,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: Colors.green,
            child: Text(
              'Đổi mật khẩu',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
