import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hues_foody/src/presentation/login_signup/login_signup.dart';
import 'package:hues_foody/src/presentation/widgets/widgets.dart';
import 'package:hues_foody/src/utils/utils.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class ForgetPasswordScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset(
                'assets/images/component-9-13@3x.png',
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Column(
                children: [
                  Header(),
                  _ForgetForm(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _ForgetForm extends StatelessWidget {
  const _ForgetForm({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String _zipCode;
    final _mask = MaskTextInputFormatter(mask: '###-###-###');

    return FormContainer(
      child: Column(
        children: [
          Text('Lấy Lại Mật Khẩu'),
          Text('Tài Khoản Qua Số Điện Thoại'),
          SizedBox(height: 10),
          Text('Chúng tôi sẽ gửi mã xác nhận OTP \n'
              'đến số điện thoại của bạn trong ít phút'),
          SizedBox(height: 20),
          PhoneCodeSelect(valueChanged: (value) => _zipCode = value),
          CustomInput(
            mask: _mask,
            type: TextInputType.phone,
            isPassword: false,
            hint: 'Số điện thoại',
          ),
          FlatButton(
            onPressed: () =>
                _submit(context, _zipCode, _mask.getUnmaskedText().toString()),
            color: Colors.green,
            minWidth: double.infinity,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Text(
              'Lấy lại mật khẩu',
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }

  _submit(BuildContext context, String zipCode, String sdt) {
    if (zipCode == null || sdt.length == 0) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Vui lòng kiểm tra lại thông tin'),
      ));
      return;
    } else {
      Navigator.pushNamed(
        context,
        Routes.otpScreen,
        arguments: OTP(isForget: true),
      );
    }
  }
}
