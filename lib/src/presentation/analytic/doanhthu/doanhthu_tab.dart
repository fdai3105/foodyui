import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:hues_foody/src/config/app_colors.dart';
import 'package:hues_foody/src/config/app_style.dart';
import 'package:hues_foody/src/presentation/presentation.dart';
import 'package:hues_foody/src/resource/models/models.dart';
import 'package:hues_foody/src/utils/utils.dart';
import 'package:intl/intl.dart';

class DoanhThuTab extends StatelessWidget {
  const DoanhThuTab({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Response.isSmallScreen(context) ? _mobile(context) : _table(context);
  }

  _mobile(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    '${DateFormat('H:m d/M/y').format(DateTime.now())}',
                    style: AppStyle.text,
                  )),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Thống kê doanh thu', style: AppStyle.title),
                  Container(
                    decoration: BoxDecoration(
                      color: AppColors.colorSecondary,
                      shape: BoxShape.circle,
                    ),
                    child: IconButton(
                      icon: Icon(Icons.settings),
                      onPressed: () {},
                      color: Colors.white,
                      padding: EdgeInsets.all(6),
                      constraints: BoxConstraints(),
                    ),
                  )
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              reverse: true,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
              child: CustomSelect(
                buttons: _calendar(10),
                onChanged: (value) {},
                defaultSelect: 0,
                selectDecoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.yellow.shade800,
                      width: 4,
                    ),
                  ),
                ),
                selectStyle: TextStyle(
                    color: Colors.yellow.shade800,
                    fontSize: 18,
                    fontWeight: FontWeight.w600),
                textStyle: TextStyle(
                  color: Colors.black54,
                  fontSize: 18,
                ),
                space: 40,
              ),
            ),
            CustomDropdown(
              hint: 'Nhà hàng',
              items: [
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 1 - Hà Nội'),
                  value: 'hanoi',
                ),
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 2 - Huế'),
                  value: 'hue',
                )
              ],
              onChanged: (value) {},
              shadow: true,
            ),
            Container(
              color: AppColors.colorSecondary,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                children: [
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(2),
                            child: Icon(
                              Icons.money_rounded,
                              color: Colors.white,
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: '200 ',
                                  style: AppStyle.titleWhite,
                                ),
                                TextSpan(
                                  text: 'triệu \n',
                                  style: AppStyle.subWhite,
                                ),
                                TextSpan(
                                  text: 'Tổng doanh thu ngày',
                                  style: AppStyle.textWhite,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(2),
                            child: Icon(
                              Icons.arrow_upward_rounded,
                              color: Colors.green,
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: '5% \n',
                                  style: AppStyle.titleWhite,
                                ),
                                TextSpan(
                                  text: 'Hôm qua',
                                  style: AppStyle.textWhite,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(2),
                            child: Icon(
                              Icons.arrow_downward_rounded,
                              color: Colors.red,
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: '2% \n',
                                  style: AppStyle.titleWhite,
                                ),
                                TextSpan(
                                  text: '1 tháng trước',
                                  style: AppStyle.textWhite,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  CustomExpansion(
                    title: 'Biểu đồ doanh thu',
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                        child: Text('ĐVT: triệu'),
                      ),
                      SizedBox(height: 10),
                      BarChartWeekly(
                        [
                          Bar(x: 0, y1: 150, y2: 80),
                          Bar(x: 1, y1: 190, y2: 150),
                          Bar(x: 2, y1: 400, y2: 80),
                          Bar(x: 3, y1: 190, y2: 60),
                          Bar(x: 4, y1: 110, y2: 190),
                          Bar(x: 5, y1: 230, y2: 130),
                          Bar(x: 6, y1: 110, y2: 130),
                        ],
                        bottomTitles: [
                          'T2',
                          'T3',
                          'T4',
                          'T5',
                          'T6',
                          'T7',
                          'CN'
                        ],
                        height: 200,
                        showGrid: true,
                        y1Title: 'Tuần này',
                        y1Color: Colors.green,
                        y2Title: 'Tuần trước',
                        unit: 'Triệu',
                        gridStep: 100,
                        divider: true,
                        columnWidth: 8,
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  CustomExpansion(
                    title: 'Biểu đồ khách hàng',
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                        child: Text('ĐVT: khách hàng'),
                      ),
                      SizedBox(height: 10),
                      BarChartWeekly(
                        [
                          Bar(x: 0, y1: 150, y2: 80),
                          Bar(x: 1, y1: 190, y2: 150),
                          Bar(x: 2, y1: 300, y2: 80),
                          Bar(x: 3, y1: 190, y2: 60),
                          Bar(x: 4, y1: 110, y2: 190),
                          Bar(x: 5, y1: 230, y2: 130),
                          Bar(x: 6, y1: 110, y2: 130),
                        ],
                        bottomTitles: [
                          'T2',
                          'T3',
                          'T4',
                          'T5',
                          'T6',
                          'T7',
                          'CN'
                        ],
                        height: 200,
                        showGrid: true,
                        y1Title: 'Tuần này',
                        y1Color: Colors.blue,
                        y2Title: 'Tuần trước',
                        unit: 'Khách hàng',
                        gridStep: 100,
                        divider: true,
                        columnWidth: 8,
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  CustomExpansion(
                    title: 'Tương quang khách hàng / doanh thu',
                    children: [
                      ChartLine(
                        lines: [
                          Line(
                            spots: [
                              FlSpot(0, 510),
                              FlSpot(1, 520),
                              FlSpot(2, 300),
                              FlSpot(3, 310),
                              FlSpot(4, 305),
                              FlSpot(5, 300),
                              FlSpot(6, 400),
                            ],
                            color: Colors.blue,
                          ),
                          Line(
                            spots: [
                              FlSpot(0, 260),
                              FlSpot(1, 460),
                              FlSpot(2, 450),
                              FlSpot(3, 430),
                              FlSpot(4, 520),
                              FlSpot(5, 490),
                              FlSpot(6, 450),
                            ],
                            color: Colors.blue.withOpacity(0.5),
                          ),
                          Line(
                            spots: [
                              FlSpot(0, 300),
                              FlSpot(1, 310),
                              FlSpot(2, 260),
                              FlSpot(3, 520),
                              FlSpot(4, 250),
                              FlSpot(5, 540),
                              FlSpot(6, 520),
                            ],
                            color: Colors.red,
                          ),
                          Line(
                            spots: [
                              FlSpot(0, 250),
                              FlSpot(1, 600),
                              FlSpot(2, 290),
                              FlSpot(3, 490),
                              FlSpot(4, 280),
                              FlSpot(5, 290),
                              FlSpot(6, 300),
                            ],
                            color: Colors.red.withOpacity(0.5),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text('Khách hàng chi tiêu nhiều nhất',
                        style: AppStyle.subWhite),
                  ),
                  SizedBox(height: 10),
                  MostSpendWidget(),
                  SizedBox(height: 20),
                  FlatButton(
                    onPressed: () {},
                    minWidth: double.infinity,
                    color: Colors.green,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text(
                      'Xuất báo cáo',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _table(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    '${DateFormat('H:m d/M/y').format(DateTime.now())}',
                    style: AppStyle.text,
                  )),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Thống kê doanh thu', style: AppStyle.title),
                  Container(
                    decoration: BoxDecoration(
                      color: AppColors.colorSecondary,
                      shape: BoxShape.circle,
                    ),
                    child: IconButton(
                      icon: Icon(Icons.settings),
                      onPressed: () {},
                      color: Colors.white,
                      padding: EdgeInsets.all(6),
                      constraints: BoxConstraints(),
                    ),
                  )
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              reverse: true,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
              child: CustomSelect(
                buttons: _calendar(10),
                onChanged: (value) {},
                defaultSelect: 0,
                selectDecoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.yellow.shade800,
                      width: 4,
                    ),
                  ),
                ),
                selectStyle: TextStyle(
                    color: Colors.yellow.shade800,
                    fontSize: 18,
                    fontWeight: FontWeight.w600),
                textStyle: TextStyle(
                  color: Colors.black54,
                  fontSize: 18,
                ),
                space: 40,
              ),
            ),
            CustomDropdown(
              hint: 'Nhà hàng',
              items: [
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 1 - Hà Nội'),
                  value: 'hanoi',
                ),
                DropdownMenuItem(
                  child: Text('Nhà hàng Hoa Mai 2 - Huế'),
                  value: 'hue',
                )
              ],
              onChanged: (value) {},
              shadow: true,
            ),
            Container(
              color: AppColors.colorSecondary,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                children: [
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(2),
                            child: Icon(
                              Icons.money_rounded,
                              color: Colors.white,
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: '200 ',
                                  style: AppStyle.titleWhite,
                                ),
                                TextSpan(
                                  text: 'triệu \n',
                                  style: AppStyle.subWhite,
                                ),
                                TextSpan(
                                  text: 'Tổng doanh thu ngày',
                                  style: AppStyle.textWhite,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(2),
                            child: Icon(
                              Icons.arrow_upward_rounded,
                              color: Colors.green,
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: '5% \n',
                                  style: AppStyle.titleWhite,
                                ),
                                TextSpan(
                                  text: 'Hôm qua',
                                  style: AppStyle.textWhite,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(2),
                            child: Icon(
                              Icons.arrow_downward_rounded,
                              color: Colors.red,
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: '2% \n',
                                  style: AppStyle.titleWhite,
                                ),
                                TextSpan(
                                  text: '1 tháng trước',
                                  style: AppStyle.textWhite,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  CustomExpansion(
                    title: 'Biểu đồ doanh thu',
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                        child: Text('ĐVT: triệu'),
                      ),
                      SizedBox(height: 10),
                      BarChartWeekly(
                        [
                          Bar(x: 0, y1: 150, y2: 80),
                          Bar(x: 1, y1: 190, y2: 150),
                          Bar(x: 2, y1: 400, y2: 80),
                          Bar(x: 3, y1: 190, y2: 60),
                          Bar(x: 4, y1: 110, y2: 190),
                          Bar(x: 5, y1: 230, y2: 130),
                          Bar(x: 6, y1: 110, y2: 130),
                        ],
                        bottomTitles: [
                          'T2',
                          'T3',
                          'T4',
                          'T5',
                          'T6',
                          'T7',
                          'CN'
                        ],
                        height: 200,
                        showGrid: true,
                        y1Title: 'Tuần này',
                        y1Color: Colors.green,
                        y2Title: 'Tuần trước',
                        unit: 'Triệu',
                        gridStep: 100,
                        divider: true,
                        columnWidth: 8,
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  CustomExpansion(
                    title: 'Biểu đồ khách hàng',
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                        child: Text('ĐVT: khách hàng'),
                      ),
                      SizedBox(height: 10),
                      BarChartWeekly(
                        [
                          Bar(x: 0, y1: 150, y2: 80),
                          Bar(x: 1, y1: 190, y2: 150),
                          Bar(x: 2, y1: 300, y2: 80),
                          Bar(x: 3, y1: 190, y2: 60),
                          Bar(x: 4, y1: 110, y2: 190),
                          Bar(x: 5, y1: 230, y2: 130),
                          Bar(x: 6, y1: 110, y2: 130),
                        ],
                        bottomTitles: [
                          'T2',
                          'T3',
                          'T4',
                          'T5',
                          'T6',
                          'T7',
                          'CN'
                        ],
                        height: 200,
                        showGrid: true,
                        y1Title: 'Tuần này',
                        y1Color: Colors.blue,
                        y2Title: 'Tuần trước',
                        unit: 'Khách hàng',
                        gridStep: 100,
                        divider: true,
                        columnWidth: 8,
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  CustomExpansion(
                    title: 'Tương quang khách hàng / doanh thu',
                    children: [
                      ChartLine(
                        lines: [
                          Line(
                            spots: [
                              FlSpot(0, 510),
                              FlSpot(1, 520),
                              FlSpot(2, 300),
                              FlSpot(3, 310),
                              FlSpot(4, 305),
                              FlSpot(5, 300),
                              FlSpot(6, 400),
                            ],
                            color: Colors.blue,
                          ),
                          Line(
                            spots: [
                              FlSpot(0, 260),
                              FlSpot(1, 460),
                              FlSpot(2, 450),
                              FlSpot(3, 430),
                              FlSpot(4, 520),
                              FlSpot(5, 490),
                              FlSpot(6, 450),
                            ],
                            color: Colors.blue.withOpacity(0.5),
                          ),
                          Line(
                            spots: [
                              FlSpot(0, 300),
                              FlSpot(1, 310),
                              FlSpot(2, 260),
                              FlSpot(3, 520),
                              FlSpot(4, 250),
                              FlSpot(5, 540),
                              FlSpot(6, 520),
                            ],
                            color: Colors.red,
                          ),
                          Line(
                            spots: [
                              FlSpot(0, 250),
                              FlSpot(1, 600),
                              FlSpot(2, 290),
                              FlSpot(3, 490),
                              FlSpot(4, 280),
                              FlSpot(5, 290),
                              FlSpot(6, 300),
                            ],
                            color: Colors.red.withOpacity(0.5),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text('Khách hàng chi tiêu nhiều nhất',
                        style: AppStyle.subWhite),
                  ),
                  SizedBox(height: 10),
                  MostSpendWidget(),
                  SizedBox(height: 20),
                  FlatButton(
                    onPressed: () {},
                    minWidth: double.infinity,
                    color: Colors.green,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text(
                      'Xuất báo cáo',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _calendar(int weeks) {
    final _list = <CustomSelectButton>[]..add(
        CustomSelectButton(
          child: Text('Tuần này'),
          value: 0,
        ),
      );
    var currentDate = DateTime.now();
    for (int i = 0; i < weeks; i++) {
      final endDate = currentDate.subtract(
        Duration(days: currentDate.weekday),
      );
      final startDate = endDate.subtract(
        Duration(days: DateTime.daysPerWeek),
      );
      currentDate = endDate;
      if (startDate.month == endDate.month) {
        _list.add(
          CustomSelectButton(
            child: Text(
                '${DateFormat('d').format(startDate)}-${DateFormat('d/M').format(endDate)}'),
            value: '${startDate.day}.${endDate.day}',
          ),
        );
      } else {
        _list.add(CustomSelectButton(
          child: Text(
              '${DateFormat('d/M').format(startDate)} - ${DateFormat('d/M').format(endDate)}'),
          value: '${startDate.day}.${endDate.day}',
        ));
      }
    }

    return _list.reversed.toList();
  }
}
