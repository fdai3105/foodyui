import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hues_foody/src/config/app_style.dart';
import 'package:hues_foody/src/presentation/widgets/widgets.dart';
import 'package:hues_foody/src/utils/utils.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class SignupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                widthFactor: 1,
                child: Image.asset(
                  'assets/images/component-9-13@3x.png',
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    Header(),
                    SignupForm(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SignupForm extends StatefulWidget {
  @override
  _SignupFormState createState() => _SignupFormState();
}

class _SignupFormState extends State<SignupForm> {
  var phoneController;
  var _zipCode;
  var termAccept;
  MaskTextInputFormatter _mask;

  @override
  void initState() {
    phoneController = TextEditingController();
    _mask = MaskTextInputFormatter(mask: '###-###-###');
    termAccept = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FormContainer(
      child: Column(
        children: [
          Text(
            'Đăng ký',
            style: AppStyle.title,
          ),
          const SizedBox(height: 10),
          Text(
            'Chúng tôi sẽ gửi mã xác nhận OTP đến \n'
            'số điện thoại của bạn trong ít phút',
            softWrap: true,
            style: AppStyle.text,
          ),
          const SizedBox(height: 20),
          PhoneCodeSelect(
            valueChanged: (value) => _zipCode = value,
          ),
          CustomInput(
            controller: phoneController,
            mask: _mask,
            type: TextInputType.phone,
            isPassword: false,
            hint: 'Số điện thoại',
          ),
          FlatButton(
            onPressed: () {
              FocusScope.of(context).unfocus();
              _submit();
            },
            color: Colors.green,
            minWidth: double.infinity,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Text(
              'Nhận OTP',
              style: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Checkbox(
                value: termAccept,
                onChanged: (value) {
                  FocusScope.of(context).unfocus();
                  setState(() {
                    termAccept = value;
                  });
                },
                activeColor: Colors.green,
              ),
              Text(
                'Điều khoản sử dụng',
                style: TextStyle(decoration: TextDecoration.underline),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _submit() {
    if (_zipCode != null && phoneController.text != null && termAccept) {
      /// validation ok
      // final _phone = _zipCode + _mask.getUnmaskedText();
      Navigator.pushReplacementNamed(context, Routes.otpScreen,
          arguments: OTP(isForget: false));
    } else {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text('Vui lòng kiểm tra lại thông tin'),
        ),
      );
    }
  }
}
